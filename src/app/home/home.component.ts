import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FaqService } from '../services/faq/faq.service';
import { GlobalStateService } from '../services/tfsGlobal/global-state.service';
import { Faq } from '../shared/models/faq';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})



export class HomeComponent implements OnInit {
  panelOpenState = false;
  faqs?: Faq[];
  elementsNumber: any;
  cookieAccepted = false;

  constructor(private router: Router, private faq_api: FaqService,
    public globalStateService: GlobalStateService) {

  }

  ngOnInit(): void {
    this.getAllFAQs();
  }

  getAllFAQs() {
    return this.faq_api.getAllFAQ().subscribe(data => {
      this.faqs = data;
      this.elementsNumber = this.faqs?.length;
    })
  }

  openAccount() {
    this.router.navigate(['offer']);
  }
  acceptCookies(): void {
    this.cookieAccepted = true;
  }

  homeRedirection(){
    this.router.navigate(['offer/validationCompte']);
  }

}

import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.scss']
})
export class CookiesComponent implements OnInit {
  @Output() cookieClick = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }
  cookieAccepted(): void {
    this.cookieClick.emit('accepted');
  }

}

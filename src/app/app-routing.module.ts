import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AccountChoiceUiSimpleStepTwoComponent } from './offer-choice/account-choice-ui-simple-step-two/account-choice-ui-simple-step-two.component';
import { AccountChoiceUiStepTwoComponent } from './offer-choice/account-choice-ui-step-two/account-choice-ui-step-two.component';
import { OfferChoiceUiStepOneComponent } from './offer-choice/offer-choice-ui-step-one/offer-choice-ui-step-one.component';
import { OfferChoiceUiComponent } from './offer-choice/offer-choice-ui/offer-choice-ui.component';
import { PersonalInfoUiStepThreeComponent } from './offer-choice/personal-info-ui-step-three/personal-info-ui-step-three.component';
import { CivilStatusComponent } from './offer-choice/personal-info/civil-status/civil-status.component';
import { CheckMailComponent } from './offer-choice/personal-info/check-mail/check-mail.component';
import { AddressInfoComponent } from './offer-choice/personal-info/address-info/address-info.component';
import { FinancialInfoComponent } from './offer-choice/personal-info/financial-info/financial-info.component';
import { AgencyComponent } from './offer-choice/personal-info/agency/agency.component';
import { RecapComponent } from './offer-choice/personal-info/recap/recap.component';
import { PersonalInfoService } from './services/personalInfo/personal-info.service';

import { CivilStatusUpdateComponent } from './offer-choice/personal-info/civil-status-update/civil-status-update.component';
import { AddressInfoUpdateComponent } from './offer-choice/personal-info/address-info-update/address-info-update.component';
import { PersonalInfoUpdateComponent } from './offer-choice/personal-info/personal-info-update/personal-info-update.component';
import { SimpleAccountUpdateComponent } from './offer-choice/personal-info/simple-account-update/simple-account-update.component';
import { AgencyInfoUpdateComponent } from './offer-choice/personal-info/agency-info-update/agency-info-update.component';
import { OfferChoiceUpdateComponent } from './offer-choice/personal-info/offer-choice-update/offer-choice-update.component';
import { CivilityStepUpdateComponent } from './offer-choice/personal-info/civility-step-update/civility-step-update.component';
import { AccountOfferUpdateComponent } from './offer-choice/personal-info/account-offer-update/account-offer-update.component';
import { TestAccordiantComponent } from './offer-choice/personal-info/test-accordiant/test-accordiant.component';
import { JustificatifComponent } from './offer-choice/personal-info/justificatif/justificatif.component';
import { ValidationMailComponent } from './offer-choice/personal-info/validation-mail/validation-mail.component';
import { CompleteRequestComponent } from './offer-choice/complete-request/complete-request.component';
import { FollowRequestStepOneComponent } from './follow-request/follow-request-step-one/follow-request-step-one.component';
import { FollowRequestStepTwoComponent } from './follow-request/follow-request-step-two/follow-request-step-two.component';
import { SignatureContratComponent } from './offer-choice/signature-contrat/signature-contrat.component';
import { PreouvertureCompteComponent } from './offer-choice/preouverture-compte/preouverture-compte.component';
import { CompteValidationComponent } from './offer-choice/compte-validation/compte-validation.component';
import { DemandSuccessComponent } from './offer-choice/demand-success/demand-success.component';
import { StepRedirectionComponent } from './follow-request/step-redirection/step-redirection.component';
import { MakeAppointmentComponent } from './offer-choice/make-appointment/make-appointment.component';
import { VisioMeetingComponent } from './offer-choice/visio-meeting/visio-meeting.component';
import { ContractComponent } from './offer-choice/contract/contract.component';
import { RequestAdvencementComponent } from './follow-request/request-advencement/request-advencement.component';
import { JustificatifsComponent } from './offer-choice/personal-info/justificatifs/justificatifs.component';



const routes: Routes = [
  { path: 'follow-request', component: FollowRequestStepOneComponent },
  { path: 'requestVerification/:id', component: FollowRequestStepTwoComponent },
  { path: 'StepRedirection/:id', component: StepRedirectionComponent },
  { path: 'requestAdvancement', component: RequestAdvencementComponent },

  { path: '', component: HomeComponent },
  {
    path: 'offer',
    component: OfferChoiceUiComponent, // this is the component with the <router-outlet> in the template
    children: [
      {
        path: '', // child route path
        component: OfferChoiceUiStepOneComponent, // child route component that the router renders
      },
      {
        path: 'step2',
        component: AccountChoiceUiStepTwoComponent, // another child route component that the router renders
      },
      {
        path: 'step2Simple',
        component: AccountChoiceUiSimpleStepTwoComponent, // another child route component that the router renders
      },
      {
        path: 'step3',
        component: PersonalInfoUiStepThreeComponent, // another child route component that the router renders
      },
      {
        path: 'check-mail',
        component: CheckMailComponent, // another child route component that the router renders
      },
      {
        path: 'civil-status/:id',
        component: CivilStatusComponent, // another child route component that the router renders
      },
      {
        path: 'address-info',
        component: AddressInfoComponent, // another child route component that the router renders
      },
      {
        path: 'financial-info',
        component: FinancialInfoComponent, // another child route component that the router renders
      },
      {
        path: 'agency-info',
        component: AgencyComponent, // another child route component that the router renders
      },
      {
        path: 'summary',
        component: RecapComponent, // another child route component that the router renders
      },
      {
        path: 'OfferChoiceUpdate',
        component: OfferChoiceUpdateComponent, // another child route component that the router renders
      },
      {
        path: 'CivilitystepUpdate',
        component: CivilityStepUpdateComponent, // another child route component that the router renders
      },
      {
        path: 'CivilStatusUpdate',
        component: CivilStatusUpdateComponent, // another child route component that the router renders
      },
      {
        path: 'AddressInfoUpdate',
        component: AddressInfoUpdateComponent, // another child route component that the router renders
      },
      {
        path: 'FinancialInfoUpdate',
        component: PersonalInfoUpdateComponent, // another child route component that the router renders
      },
      {
        path: 'AgencyInfoUpdate',
        component: AgencyInfoUpdateComponent, // another child route component that the router renders
      },
      {
        path: 'SimpleAccountUpdate',
        component: SimpleAccountUpdateComponent, // another child route component that the router renders
      },
      {
        path: 'AccountOfferUpdate',
        component: AccountOfferUpdateComponent, // another child route component that the router renders
      },

      {
        path: 'acc',
        component: TestAccordiantComponent, // another child route component that the router renders
      },
      {
        path: 'justificatifs',
        component: JustificatifComponent, // another child route component that the router renders
      },
      {
        path: 'justificatif',
        component: JustificatifsComponent, // another child route component that the router renders
      },
      {
        path: 'mailValidation',
        component: ValidationMailComponent, // another child route component that the router renders
      },
      {
        path: 'completeRequest',
        component: CompleteRequestComponent, // another child route component that the router renders
      },
      {
        path: 'signatureContrat',
        component: SignatureContratComponent, // another child route component that the router renders
      },
      {
        path: 'preouvertureCompte',
        component: PreouvertureCompteComponent, // another child route component that the router renders
      },
      {
        path: 'validationCompte',
        component: CompteValidationComponent, // another child route component that the router renders
      },
      {
        path: 'demandSuccess',
        component: DemandSuccessComponent, // another child route component that the router renders
      },
      {
        path: 'makeAppointment',
        component: MakeAppointmentComponent, // another child route component that the router renders
      },
      {
        path: 'visioMeeting',
        component: VisioMeetingComponent, // another child route component that the router renders
      },
      {
        path: 'contract',
        component: ContractComponent, // another child route component that the router renders
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [PersonalInfoService]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss']
})
export class TranslateComponent implements OnInit {

  constructor(public translate: TranslateService) {

    const lang = localStorage.getItem('lang');
    /** si l'utilisateur a deja choisi une langue, on utlise son choix
     * sinon celui par défaut
     */
    if (lang === 'en') {
      translate.addLangs(['en', 'fr']);
      translate.setDefaultLang('en');
    }
    else {
      translate.addLangs(['fr', 'en']);
      translate.setDefaultLang('fr');
    }
  }

  ngOnInit(): void {
  }


  switchLang(lang: string) {
    this.translate.setDefaultLang(lang);
    localStorage.setItem('lang', lang);
    this.translate.use(lang);
  }

}

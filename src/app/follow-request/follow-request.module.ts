import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FollowRequestStepOneComponent } from './follow-request-step-one/follow-request-step-one.component';
import { FollowRequestStepTwoComponent } from './follow-request-step-two/follow-request-step-two.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {TranslateModule} from '@ngx-translate/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StepRedirectionComponent } from './step-redirection/step-redirection.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { RequestAdvencementComponent } from './request-advencement/request-advencement.component';
import {MatButtonModule} from '@angular/material/button';
import {MatStepperModule} from '@angular/material/stepper';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {SharedModule} from '../shared/shared.module';
import { MaterialModule } from '../material/material.module';
import { TooltipModule } from 'ng2-tooltip-directive';



@NgModule({
  declarations: [FollowRequestStepOneComponent, FollowRequestStepTwoComponent, StepRedirectionComponent, RequestAdvencementComponent],
    imports: [
        CommonModule,
        MatToolbarModule,
        TranslateModule,
        FlexLayoutModule,
        FormsModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatStepperModule,
        MatInputModule,
        ReactiveFormsModule,
        MatCardModule,
        MaterialModule,
        SharedModule,
        TooltipModule
    ]
})
export class FollowRequestModule { }

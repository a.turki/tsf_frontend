import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestAdvencementComponent } from './request-advencement.component';

describe('RequestAdvencementComponent', () => {
  let component: RequestAdvencementComponent;
  let fixture: ComponentFixture<RequestAdvencementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestAdvencementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestAdvencementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

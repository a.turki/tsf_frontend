import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowRequestStepOneComponent } from './follow-request-step-one.component';

describe('FollowRequestStepOneComponent', () => {
  let component: FollowRequestStepOneComponent;
  let fixture: ComponentFixture<FollowRequestStepOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FollowRequestStepOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowRequestStepOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

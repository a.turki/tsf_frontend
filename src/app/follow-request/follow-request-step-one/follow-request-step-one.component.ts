import { Component, OnInit } from '@angular/core';
import {RequestService} from '../../services/request/request.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-follow-request-step-one',
  templateUrl: './follow-request-step-one.component.html',
  styleUrls: ['./follow-request-step-one.component.scss']
})
export class FollowRequestStepOneComponent implements OnInit {
  myOptions = {
    'placement': 'right',
    'show-delay': 500,
    'hide-delay': 300,
    'max-width': 200,
    'width': 200,
    'animationDuration': 300,
	'animationDurationDefault': 300
}
  errors = false;
  requestId: any;
  constructor(private requestService: RequestService,
              private router: Router  ) { }

  ngOnInit(): void {
  }


  followRequest(): void {
  console.log(this.requestId);
  this.requestService.followRequest(this.requestId).subscribe(
    (data) => {
      console.log(data);
      this.router.navigate(['requestVerification' , this.requestId]);

    },
    (err: any) => this.errors = true,
    () => {
      console.log('done');
    }
  );

  }
  
  homeRedirection() {
    this.router.navigate(['']);
  }

}

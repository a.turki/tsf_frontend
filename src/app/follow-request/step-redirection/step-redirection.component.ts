import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestService} from '../../services/request/request.service';
import {GlobalStateService} from '../../services/tfsGlobal/global-state.service';

@Component({
  selector: 'app-step-redirection',
  template: `
    <mat-spinner></mat-spinner>

  `,
  styleUrls: ['./step-redirection.component.scss']

})
export class StepRedirectionComponent implements OnInit {
  reqId: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private requestService: RequestService,
              public globalStateService: GlobalStateService) { }

  ngOnInit(): void {
    this.reqId = this.route.snapshot.paramMap.get('id');
    this.requestService.getRequestById(this.reqId).subscribe(data => {
        console.log(data);
        switch (data.step) {
          case 'civil-status':
            this.router.navigate(['offer/civil-status/' + this.reqId]);
            break;
          case 'address-info':
            this.router.navigate(['offer/address-info']);
            break;
          case 'financial-info':
            this.router.navigate(['offer/financial-info']);
            break;
          case 'agency-info':
            this.router.navigate(['offer/agency-info']);
            break;
          default:
            this.router.navigate(['']);
        }


      },
      (err: any) => console.log(err),
      () => {
        console.log('done');
      });
  }

}

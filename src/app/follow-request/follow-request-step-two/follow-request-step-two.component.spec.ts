import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowRequestStepTwoComponent } from './follow-request-step-two.component';

describe('FollowRequestStepTwoComponent', () => {
  let component: FollowRequestStepTwoComponent;
  let fixture: ComponentFixture<FollowRequestStepTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FollowRequestStepTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowRequestStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {RequestService} from '../../services/request/request.service';

@Component({
  selector: 'app-follow-request-step-two',
  templateUrl: './follow-request-step-two.component.html',
  styleUrls: ['./follow-request-step-two.component.scss']
})
export class FollowRequestStepTwoComponent implements OnInit {
  reqId: any;
  code: any;
  errors = false;
  myOptions = {
    'placement': 'right',
    'show-delay': 500,
    'hide-delay': 300,
    'max-width': 200,
    'width': 200,
    'animationDuration': 300,
	'animationDurationDefault': 300
}
  constructor(private route: ActivatedRoute,
              private router: Router,
              private requestService: RequestService) { }

  ngOnInit(): void {
    this.reqId = this.route.snapshot.paramMap.get('id');

  }

  verifyCode(): void {
    this.requestService.verifyCode(this.reqId, this.code).subscribe(data => {
      console.log(data);
      if (data === null) {
        this.errors = true;
      }
      else {
        this.router.navigate(['StepRedirection' , this.reqId]);
      }
    },
      (err: any) => console.log(err),
      () => {
        console.log('done');
      });
  }
  
  homeRedirection() {
    this.router.navigate(['']);
  }

}

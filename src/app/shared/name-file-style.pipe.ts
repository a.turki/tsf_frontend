import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameFileStyle'
})
export class NameFileStylePipe implements PipeTransform {

  transform(value: any): unknown {
    if (!value)
        return null;
       return value.substr(12,30); 
    }
    
    
}

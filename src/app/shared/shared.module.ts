import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateComponent } from '../translate/translate.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';

import { MaterialModule } from '../material/material.module';
import { NameFileStylePipe } from './name-file-style.pipe';
import { ResponsiveTooltipComponent } from './components/responsive-tooltip/responsive-tooltip.component';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexModule} from '@angular/flex-layout';
import { UploadInputComponent } from './components/upload-input/upload-input.component';
import {NgxDropzoneModule} from 'ngx-dropzone';

@NgModule({
  declarations: [TranslateComponent, NameFileStylePipe, ResponsiveTooltipComponent, UploadInputComponent],
    imports: [

        CommonModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: httpTranslateLoader,
                deps: [HttpClient]
            }
        }),
        MatButtonModule,
        MatDialogModule,
        FlexModule,
        NgxDropzoneModule
    ],
  exports: [TranslateComponent, TranslateModule, NameFileStylePipe, UploadInputComponent]
})
export class SharedModule { }

// AOT compilation support
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

import { Account } from './account';
import { Offer } from './offer';
import { PersonalInfo } from './personalInfo';
export class UserChoice {
    offer = new Offer();
    personalInfo = new PersonalInfo();
    recup = '';
    justif = '';
    sign = '';
    accounts!: Account[];
    progressBarValue!: number;
    _step!: string;

}

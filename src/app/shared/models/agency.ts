export class Agency {
    id?: number;
    personalInfoId?: number;
    name?: string;
    address?: string;
    zip?: number;
    city?: string;
    municipalityId?:number;
    constructor(name?: string, address?: string, zip?: number,
        city?: string) {
            this.name = name;
            this.address = address;
            this.zip = zip;
            this.city = city;

    }
}
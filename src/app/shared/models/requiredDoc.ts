export class RequiredDoc {
    id? : number;
    label?: string;
    type?: string;
    numCIN?: string;
    deliveryDateCin?:Date;
    rectoCin?: string;
    versoCin?: string;
    fatca?: string;
    
  }
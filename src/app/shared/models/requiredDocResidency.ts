export class RequiredDocResidency{
    id? : number;
    type?: string;
    numCIN?: string;
    deliveryDate?:Date;
    experationDate?:Date;
    illimitedExperationDate?:boolean;
    residencyRecto?: string;
    residencyVerso?: string;
    }
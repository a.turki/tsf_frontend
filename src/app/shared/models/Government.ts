import { Municipality } from "./Municipality";

export class Governement {
    id?: number;
    name?: string;
    municipalities?: Municipality[];
    constructor(id?: number,  name?: string) {
        this.id = id;
        this.name = name;
    }
}
export class AuthentificationToSign {
    id?: number;
    email?: string;
    token?: string;
    dateCreation?: Date;
    valide?: boolean;
}



export class AddressInfo {
    id?:number;
    personalInfoId?: number;
    countryOfResidence?: string;
    address?: string;
    zip?:number;
    city?: string;

    constructor(countryOfResidence?: string, address?: string,
        zip?: number, city?: string) {
            this.countryOfResidence = countryOfResidence;
            this.address = address;
            this.zip = zip;
            this.city = city;

    }
}
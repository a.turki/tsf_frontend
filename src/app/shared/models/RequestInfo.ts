import { Account } from "./account";

export class RequestInfo {
    id?:number;
    visioDate?:Date;
    sendingMailDate?:Date;
    state?:boolean;
    offerId?:number;
    bankAccounts?: Account[];
    constructor(id?: number, visioDate?: Date, sendingMailDate?: Date, state?: boolean,
        offerId?: number) {
        this.id = id;
        this.visioDate = visioDate;
        this.sendingMailDate = sendingMailDate;
        this.state = state;
        this.offerId = offerId;
    }
}

import { Agency } from './agency';
export class Municipality {
    id?: number;
    name?: string;
    governorateId?: number;
    agencies?: Agency[]
    
    constructor(id?: number,  name?: string) {
        this.id = id;
        this.name = name;
    }
}
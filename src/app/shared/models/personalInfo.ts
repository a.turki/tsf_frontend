import { Civility } from "./civility";
import { Agency } from "./agency";
import { FinancialInfo } from "./financialInfo";
import { AddressInfo } from "./addressInfo";
export class PersonalInfo {
id?:number;
civility?:Civility;
firstName?:string;
lastName?:string;
email?:string;
phone?:string;
nativeCountry?:string;
birthday?:Date;
clientABT?:boolean;
rib?:string;
nationality?:string;
secondNationality?:string;
maritalStatus?:string;
nbrkids?:number;
americanIndex?:boolean;
addressInfo?: AddressInfo;
financialInfo?: FinancialInfo;
agencyId?: number;
agency?: Agency;
activated!:string;

constructor(id?: number, civility?: Civility, firstName?: string, lastName?: string, email?: string,
    phone?: string, nativeCountry?: string, birthday?: Date, clientABT?: boolean, rib?: string,
    nationality?: string, secondNationality?: string, maritalStatus?: string, nbrkids?: number,
    americanIndex?: boolean, activated?: any) {
        this.id = id;
        this.civility = civility;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.nativeCountry = nativeCountry;
        this.birthday = birthday;
        this.clientABT = clientABT;
        this.rib = rib;
        this.nationality = nationality;
        this.secondNationality = secondNationality;
        this.maritalStatus = maritalStatus;
        this.nbrkids = nbrkids;
        this.americanIndex = americanIndex;
        this.activated = activated;
    }
}
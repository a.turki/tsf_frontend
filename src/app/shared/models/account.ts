export class Account {
  id? : number;
  libelleFR?: string;
  libelleEN?: string;
  descriptionFR?: string;
  descriptionEN?: string;
  checked?:boolean
}

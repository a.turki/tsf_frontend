
export class InformationFinancier {
    id?: number;
    activityNameFR?: string;
    activityNameEN?: string;
    categoryNameFR?: string;
    categoryNameEN?: string;
    incomesFR?: string;
    incomesEN?: string;
}
import { DetailOffers } from './detailOffers';
export class Offer {
    id?: number;
    name?: string;
    price?: number;
    url?: string;
    detailOffers?: DetailOffers[];
    activated!: string;
}

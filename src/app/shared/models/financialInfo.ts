export class FinancialInfo {
    id?: number;
    personalInfoId?: number;
    monthlyNetIncomeId?: number;
    categoryId?: number;
    activityId?: number;

    constructor(id?: number, personalInfoId?: number, monthlyNetIncomeId?: number, categoryId?: number,
        activityId?: number) {
        this.id = id;
        this.personalInfoId = personalInfoId;
        this.monthlyNetIncomeId = monthlyNetIncomeId;
        this.categoryId = categoryId;
        this.activityId = activityId;
    }
}
import { Account } from 'src/app/shared/models/account';
import { Civility } from 'src/app/shared/models/civility';


export class UserInfoChoice  {
  civility?: Civility;
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: number;
  nativeCountry?: string;
  birthday?: Date;
  accounts?: Account[];
  langue?:string
};

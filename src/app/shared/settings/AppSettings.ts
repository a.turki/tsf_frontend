

  // Temp
  export const CITIES = ['Vaudeville', 'Paris (Paris)',
  'Marseille (Bouches-du-Rhône)',
  'Toulouse (Haute-Garonne)',
  'Nice (Alpes-Maritimes)',
  'Nantes (Loire-Atlantique)',
  'Montpellier (Hérault)',
  'Strasbourg (Bas-Rhin)',
  'Bordeaux (Gironde)',
  'Lille (Nord)',
  'Rennes (Ille-et-Vilaine)',
  'Reims (Marne)',
  'Saint-Étienne (Loire)',
  'Toulon (Var)',
  'Le Havre (Seine-Maritime)',
  'Grenoble (Isère)',
  'Dijon (Côte-d\'Or)',
  'Angers (Maine-et-Loire)',
  'Nîmes (Gard)',
  'Saint-Denis (La Réunion)',
  'Villeurbanne (Rhône)',
  'Clermont-Ferrand (Puy-de-Dôme)',
  'Le Mans (Sarthe)',
  'Aix-en-Provence (Bouches-du-Rhône)',
  'Brest (Finistère)',
  'Tours (Indre-et-Loire)',
  'Amiens (Somme)',
  'Limoges (Haute-Vienne)',
  'Annecy (Haute-Savoie)',
  'Perpignan (Pyrénées-Orientales)'];

export class AppSettings {
   
}
export let ls = {
  set: function (item:any, e:any) {
      localStorage.setItem(item,e)
  },
  get: function (e:any)  {
      return localStorage.getItem(e);
  },
  remove: function (e:any) {
      localStorage.removeItem(e);
  },
  clear: function () {
      localStorage.clear();
  }
};
import {Component, ElementRef, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {FileUploadService} from '../../../services/file-upload/file-upload.service';

@Component({
  selector: 'app-upload-input',
  templateUrl: './upload-input.component.html',
  styleUrls: ['./upload-input.component.scss']
})
export class UploadInputComponent implements OnInit {
  @Input() docType!: string;
  @Input() reqId!: number;
  files: File[] = [];
  fileType = 'image/jpeg,image/jpg,image/png,image/gif,application/pdf';
  maxFileSize = 10000000; // bytes
  isFileSelected = false;
  id!: number;
  constructor(
    private hostElement: ElementRef,
    private fileUploadService: FileUploadService
              ) { }

  ngOnInit(): void {
  }
  onSelect(event: any): void {
    console.log(event);
    console.log(this.files.length);
    if (this.files.length >= 1)
    {
      this.onRemove(event);
    }
    this.files.push(...event.addedFiles);
    this.isFileSelected = true;
    this.fileUploadService.uploadFile(event.addedFiles[0], this.reqId, this.docType).subscribe(
      (data:any) => {
        this.id = data.id;
        console.log(data);
      }
    );
  }

  onRemove(event: any): void {
    console.log(event);
    this.fileUploadService.delete(this.id).subscribe(data => {
      if (data === true){
        this.files.splice(this.files.indexOf(event), 1);
        this.isFileSelected = false;
      }
    });
   }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsiveTooltipComponent } from './responsive-tooltip.component';

describe('ResponsiveTooltipComponent', () => {
  let component: ResponsiveTooltipComponent;
  let fixture: ComponentFixture<ResponsiveTooltipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResponsiveTooltipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsiveTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

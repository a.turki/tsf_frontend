import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { OfferChoiceModule } from './offer-choice/offer-choice.module';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxFlagPickerModule } from 'ngx-flag-picker';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FollowRequestModule } from './follow-request/follow-request.module';
import { CookiesComponent } from './cookies/cookies.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {JoyrideModule} from 'ngx-joyride';
import { TooltipModule } from 'ng2-tooltip-directive';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CookiesComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    OfferChoiceModule,
    FollowRequestModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    SharedModule,
    NgxFlagPickerModule,
    MatProgressSpinnerModule,
    PdfViewerModule,
    TooltipModule,
    JoyrideModule.forRoot(),


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

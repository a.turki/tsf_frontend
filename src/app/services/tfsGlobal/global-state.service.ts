import { Injectable } from '@angular/core';
import { UserChoice } from 'src/app/shared/models/userChoice';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
export interface Country {
  name: string;
  nationality: string;
  flag: string;

}

export class HeaderButton {
  label = '';
  btnClassName = '';
  btnContentClassName = '';
  routerName = '';
  constructor(label: string, btnClassName: string, btnContentClassName: string, routerName: string) {
    this.label = label;
    this.btnClassName = btnClassName;
    this.btnContentClassName = btnContentClassName;
    this.routerName = routerName;

  }
}

@Injectable({
  providedIn: 'root',
})
export class GlobalStateService  {


  userChoice!: UserChoice;
  constructor(private http: HttpClient, public translate: TranslateService) {
    this.userChoice = new UserChoice();
  }
  getHeaderButton(screenWidth: any): HeaderButton  {
      if (screenWidth < 500) {
        if (this.userChoice._step === 'offer_choice') {

          return new HeaderButton('Ouvrir un compte simple', 'orange-btn', 'orange-btn-content', 'ouvrircomptesimple');
        } else {
          return new HeaderButton('', 'btn-none', '', '');

        }
      } else {
      if (this.userChoice._step === 'offer_choice') {
    return new HeaderButton('Ouvrir un compte simple', 'orange-btn', 'orange-btn-content', 'ouvrircomptesimple');
  }
      if (this.userChoice._step === 'target_account') {
    return new HeaderButton('', 'btn-none', '', '');
  }
      if (this.userChoice._step === 'simple_target_account') {
    return new HeaderButton('', 'btn-none', '', '');
  }
      if (this.userChoice._step === 'validation_account') {
    return new HeaderButton('', 'btn-none', '', '');
  }
      if (this.userChoice._step === 'information') {
    return new HeaderButton('', 'btn-none', '', '');
  }
  if (this.userChoice._step === 'validationCompte') {
    return new HeaderButton('', 'btn-none', '', '');
  }
}
      return new HeaderButton('Enregistrer et quitter', 'fancy-btn', 'fancy-btn-content', 'mailValidation');

  }

  getStepNumber(): string {
    if (this.userChoice.sign !== ''){
      return '5';
    }
    if (this.userChoice.justif !== ''){
      return '4';
    }
    if (this.userChoice.recup !== ''){
      return '3';
    }
    if (this.userChoice.personalInfo.activated !== undefined && this.userChoice.personalInfo.activated !== ''){
      return '2';
    }
    return '1';
  }

  getStepName(): string {
    if (this.userChoice.sign !== ''){
      return 'stepperSign';
    }
    if (this.userChoice.justif !== ''){
      return 'stepperDocs';
    }
    if (this.userChoice.recup !== ''){
      return 'stepperSum';
    }
    if (this.userChoice.personalInfo.activated !== undefined && this.userChoice.personalInfo.activated !== ''){
      return 'stepperInfo';
    }
    return 'stepperOffer';
  }
}

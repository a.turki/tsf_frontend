import { TestBed } from '@angular/core/testing';

import { TfsApiService } from './tfs-api.service';

describe('TfsApiService', () => {
  let service: TfsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TfsApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

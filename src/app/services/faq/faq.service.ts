import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APP_URL } from 'src/app/shared/settings/Constants';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor(private http:HttpClient) { }

  getAllFAQ() : Observable<any>{
    return this.http.get(APP_URL+"faqs");
  }
}

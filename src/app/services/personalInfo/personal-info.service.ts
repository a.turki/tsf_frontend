import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { APP_URL, GET_MONTHLYNETINCOME_URL, GET_MUN_URL, GET_PERSONAL_INFO, GET_REQUEST, PERSONAL_INFO_URL, UPDATE_PERSONAL_INFO_CIVILITY, UPDATE_REQUEST } from 'src/app/shared/settings/Constants';
import { SAVE_REQUEST } from 'src/app/shared/settings/Constants';
import { UPDATE_PERSONAL_ADDESS } from 'src/app/shared/settings/Constants';
import { UPDATE_PERSONAL_FINANCIAL_INFOS } from 'src/app/shared/settings/Constants';
import { UPDATE_PERSONAL_AGENCY } from 'src/app/shared/settings/Constants';
import { PersonalInfo } from 'src/app/shared/models/personalInfo';
import { AddressInfo } from 'src/app/shared/models/addressInfo';
import { FinancialInfo } from 'src/app/shared/models/financialInfo';
import { Agency } from 'src/app/shared/models/agency';
import { GlobalStateService } from '../tfsGlobal/global-state.service';
import { UserInfoChoice } from 'src/app/shared/types/UserInfoChoice';
import { GET_CITIES_URL, GET_CATEGORIES_URL, GET_ACTIVITIES_URL, GET_GOV_URL }
  from 'src/app/shared/settings/Constants';
import { Router } from '@angular/router';
import { RequestInfo } from 'src/app/shared/models/RequestInfo';
const httpOptionsPlain = {
  headers: new HttpHeaders({
    'Accept': 'text/plain',
    'Content-Type': 'text/plain'
  }),
  'responseType': 'text'
};
@Injectable()
export class PersonalInfoService {
  offerId!: any;

  constructor(private http: HttpClient, public globalStateService: GlobalStateService,
    private router: Router) {

  }

  save(personalInfoForm: UserInfoChoice): Observable<any> {
    this.offerId = this.globalStateService.userChoice.offer.id;
    console.log(this.offerId)
    if (this.offerId === null || this.offerId === undefined) {
      console.warn('111111')
      return this.http.post<any>("http://localhost:9090/api/addRequest", personalInfoForm).pipe(
        catchError((err) => {
          console.error(err);
          throw err;
        }
        ))

     
    }
    else {
      console.warn('222222')
      const regex = 'offer_id';
      const URL = SAVE_REQUEST.replace(regex, this.offerId);
      return this.http.post<any>(URL, personalInfoForm).pipe(
        catchError((err) => {
          console.error(err);
          throw err;
        }
        ))
    }
  }

  update(personalInfo: PersonalInfo): Observable<any> {
    return this.http.put<any>(UPDATE_PERSONAL_INFO_CIVILITY, personalInfo).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ))
  }
  updateAddress(addressInfo: AddressInfo): Observable<any> {
    return this.http.put<any>(UPDATE_PERSONAL_ADDESS, addressInfo).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ))
  }
  updateAdd(addressInfo: AddressInfo): Observable<any> {
    return this.http.post<any>(UPDATE_PERSONAL_ADDESS, addressInfo).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ))
  }
  updateFinancialInfo(financialInfo: FinancialInfo): Observable<any> {
    return this.http.post<any>(UPDATE_PERSONAL_FINANCIAL_INFOS, financialInfo).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }
  updateFinancialIn(financialInfo: FinancialInfo): Observable<any> {
    return this.http.put<any>(UPDATE_PERSONAL_FINANCIAL_INFOS, financialInfo).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }
  updateAgency(agency: Agency): Observable<any> {
    return this.http.put<any>(UPDATE_PERSONAL_AGENCY, agency).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }


  getCities(country: string): Observable<any> {
    const regex = 'countryId';
    const URL = GET_CITIES_URL.replace(regex, country);
    return this.http.get(URL);
  }

  getCategories(): Observable<any> {
    return this.http.get(GET_CATEGORIES_URL);
  }

  getGovernements(): Observable<any> {
    return this.http.get(GET_GOV_URL);
  }
  getMunicipalities(): Observable<any> {
    return this.http.get(GET_MUN_URL);
  }

  getActivities(): Observable<any> {
    return this.http.get(GET_ACTIVITIES_URL);
  }

  getMonthlyNetIncomes(): Observable<any> {
    return this.http.get(GET_MONTHLYNETINCOME_URL);
  }

  getRequest(id: number): Observable<any> {
    return this.http.get(GET_REQUEST + id).pipe(
      catchError((err) => {
        console.error(err);
        this.router.navigate(['']);
        throw err;
      }
      ));
  }

  findByRequestId(id: number): Observable<any> {
    return this.http.get(GET_PERSONAL_INFO + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));

  }
  findByPersonalId(id: number): Observable<any> {
    return this.http.get(PERSONAL_INFO_URL + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }

  findAddress(id: number): Observable<any> {
    return this.http.get(APP_URL + "findAddress/" + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }

  findFinancialInfo(id: number): Observable<any> {
    return this.http.get(APP_URL + "findFinancialInfo/" + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }

  findAgency(id: number): Observable<any> {
    return this.http.get(APP_URL + "findAgency/" + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }


  updateRequest(request: RequestInfo): Observable<any> {
    return this.http.put<any>(UPDATE_REQUEST, request).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }
  getFinancialInfo(id: number): Observable<any> {
    return this.http.get(APP_URL + "getFinancialInfo/" + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }
  getCategory(id: any): Observable<any> {
    return this.http.get(APP_URL + "categories/" + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }

  getAgency(id: any): Observable<any> {
    return this.http.get(APP_URL + "agencies/" + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }

  getGoverment(id: any): Observable<any> {
    return this.http.get(APP_URL + "governorates/" + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }

  getMunicipalite(id: any): Observable<any> {
    return this.http.get(APP_URL + "municipalities/" + id).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }





  RequestRegistration(id: any, step: any): Observable<any> {
    return this.http.get(APP_URL + "RequestRegistration/" + id + "/" + step).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }

  AddDocument(id: any): Observable<any> {
    return this.http.post<any>("http://localhost:9090/api/documents/" + id, {}).pipe(
      catchError((err) => {
        console.error(err);
        throw err;
      }
      ));
  }

  goToSignDocument(idDeblocage: number, offerId: any): Observable<any> {
    if (offerId === null) {
      return this.http.post<any>("http://localhost:9090/api/addDocumentCompte/" + idDeblocage, {}).pipe(
        catchError((err) => {
          console.error(err);
          throw err;
        }
        ));
    }
    else {
      return this.http.post<any>("http://localhost:9090/api/addDocument/" + idDeblocage, {}).pipe(
        catchError((err) => {
          console.error(err);
          throw err;
        }
        ));
    }
  }

  getDocumentSigned(idDossier: string) {
    return this.http.get("http://localhost:9090/api/getDocument/" + idDossier);
  }


  downloadFile(idOffer: number) {
    console.log(idOffer)
    if (idOffer === null) {
      return this.http.request('GET', "http://localhost:9090/api/download", { responseType: 'blob' as 'json' }).pipe(
        catchError((err) => {
          console.error(err);
          throw err;
        }
        ));
    }
    else {
      return this.http.request('GET', "http://localhost:9090/api/downloadFile?idOffer=" + idOffer, { responseType: 'blob' as 'json' }).pipe(
        catchError((err) => {
          console.error(err);
          throw err;
        }
        ));
    }
  }



  saveTokenToSign(email: string) {
    return this.http.get("http://localhost:9090/api/saveTokenToSign/" + email);
  }


  getSubsciberSatus(idRequest: number) {
    return this.http.get("http://localhost:9090/api/getSubsciberSatus/" + idRequest);
  }

}
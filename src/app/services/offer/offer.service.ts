import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Offer } from '../../shared/models/offer';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { APP_URL } from 'src/app/shared/settings/Constants';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

   

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  } 
  constructor(private http: HttpClient) { }


  getOffers(): Observable<Offer[]> {
    return this.http.get<Offer[]>( APP_URL + 'offers')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }


  handleError(error: any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
   // window.alert(errorMessage);
    return throwError(errorMessage);
 }


 getOfferByReqId(reqId:number): Observable<any> {
  return this.http.get<any>( APP_URL + 'offers/findByReqId/'+reqId)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}
}

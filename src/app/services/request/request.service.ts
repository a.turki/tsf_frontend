import { Injectable } from '@angular/core';
import {GET_FOLLOW_REQUEST, GET_REQUEST_BY_ID, GET_VERIFY_CODE} from 'src/app/shared/settings/Constants';
import {HttpClient} from '@angular/common/http';
import {GlobalStateService} from '../tfsGlobal/global-state.service';
import {Router} from '@angular/router';
import {UserInfoChoice} from '../../shared/types/UserInfoChoice';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  followRequest(idRequest: any): Observable<any> {
    return this.http.get<any>(GET_FOLLOW_REQUEST + idRequest).pipe(
      catchError((err) => {
          console.error(err);
          throw err;
        }
      ));
  }
  getRequestById(idRequest: any): Observable<any> {
    return this.http.get<any>(GET_REQUEST_BY_ID + idRequest).pipe(
      catchError((err) => {
          console.error(err);
          throw err;
        }
      ));
  }
  verifyCode(idRequest: any, code: any): Observable<any> {
    return this.http.get<any>(GET_VERIFY_CODE + idRequest + '/' + code).pipe(
      catchError((err) => {
          console.error(err);
          throw err;
        }
      ));
  }
}

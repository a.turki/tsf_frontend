import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JustificatifsService {
  URL="http://localhost:9090/api/";
  constructor(private http: HttpClient) { }
  headers = new HttpHeaders();
  
uploadFileAuther(file: File,requestId:any,typeDoc:any): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('requestId', requestId);
    formData.append('typeDoc', typeDoc);
   const req = new HttpRequest('POST', `${this.URL}updateFileData`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
  uploadFile(file: File,requestId:any,typeDoc:any): Observable<HttpEvent<any>> {
   
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('requestId', requestId);
    formData.append('typeDoc', typeDoc);
    
    
   const req = new HttpRequest('POST', `${this.URL}updateFile`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
  uploadFileRevenu(file: File,requestId:any): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('requestId', requestId);
    const req = new HttpRequest('POST', `${this.URL}uploadFileRevenu`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
  uploadResidency(file: File,requestId:any): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('requestId', requestId);
    
    const req = new HttpRequest('POST', `${this.URL}uploadFileResidency`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
  getAllResidencyDocument ():Observable<any>{
    return this.http.get<any>(`${this.URL}residency-documents` )
   
  }
  getAllRevenuDocumentById (id:number):Observable<any>{
    return this.http.get<any>(`${this.URL}other-revenu-files-by-requestId/${id}` )
   
  }
  getNasionaliteByRequestId (id:number):Observable<any>{
    return this.http.get<any>(`${this.URL}required-doc-incomes/${id}` )
   
  }
  getAllResidencyDocumentById (id:number):Observable<any>{
    return this.http.get<any>(`${this.URL}other-residency-files-by-requestId/${id}` )
   
  }
  getJustifById(id:number):Observable<any>{
    return this.http.get<any>(`${this.URL}get-all-data-of-doc/${id}` )

  }
  
  saveRequiredDoc(data: any,id:any): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    const options = { headers: this.headers };
    const req = new HttpRequest('POST', `${this.URL}required-docs/${id}`,data , {
      reportProgress: true,
      responseType: 'json'
    });

   return this.http.request(req);;
  }
  getDoc(requestId: number): Observable<Object> {  
    return this.http.get(`${this.URL}required-docs-by-request/${requestId}`);  
  } 
  getDocResideny(requestId: number): Observable<Object> {  
    return this.http.get(`${this.URL}required-doc-residencies-by-requestId/${requestId}`);  
  } 
 getAllJustifRevenu(){
  return this.http.get<any>(`${this.URL}justif-revenus` )
 }
 
 deleteFile(requestId: number,typeDoc:String): Observable<Object> {  
    return this.http.get(`${this.URL}deleteFile/${requestId}/${typeDoc}`);  
   } 


deleteRevenu(id: number): Observable<any> {  
  return this.http.delete(`${this.URL}other-revenu-files/${id}`, { responseType: 'text' });  
} 
deleteResidency(id: number): Observable<any> {  
  return this.http.delete(`${this.URL}other-residency-files/${id}`, { responseType: 'text' });  
} 


public downloadFile(reqId:any,id:any) :Observable<any>{		
  return this.http.get(`${this.URL}required-docs/downloadFile/${reqId}/${id}`,  { responseType: 'blob' })
 } 

 public downloadFileAutherResidency  (id:any) :Observable<any>{		
  return this.http.get(`${this.URL}required-docs/downloadfileResidency/${id}`,  { responseType: 'blob' })
 } 
 public downloadFileRevnu (id:any) :Observable<any>{		
  return this.http.get(`${this.URL}required-docs/downloadfileRevenu/${id}`,  { responseType: 'blob' })
 } 
 public downloadFileFatca () :Observable<any>{		
  return this.http.get(`${this.URL}required-docs/downloadfilefatca/`,  { responseType: 'blob' })
 } 
 

  }

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GET_ACCOUNTS } from 'src/app/shared/settings/Constants';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http:HttpClient) {

  }
  getAllAccounts() : Observable<any>{
    return this.http.get(GET_ACCOUNTS);
  }
}

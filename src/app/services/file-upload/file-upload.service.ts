import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {APP_URL, DELETE_REQ_DOC, UPLOAD_REQ_DOC} from '../../shared/settings/Constants';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private http: HttpClient) { }

  uploadFile(file: File, requestId: any, docType: any): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('requestId', requestId);
    formData.append('docType', docType);

   /* const req = new HttpRequest('POST', UPLOAD_REQ_DOC, formData, {
      reportProgress: true,
      responseType: 'json'
    });*/
    return this.http.post<any>(UPLOAD_REQ_DOC, formData,{
      reportProgress: true,
      responseType: 'json'
    });

  }
  delete(id: any): Observable<any>{
    return this.http.delete(DELETE_REQ_DOC + id);
  }
}

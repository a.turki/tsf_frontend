import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisioMeetingComponent } from './visio-meeting.component';

describe('VisioMeetingComponent', () => {
  let component: VisioMeetingComponent;
  let fixture: ComponentFixture<VisioMeetingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisioMeetingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisioMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account/account.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { Account } from 'src/app/shared/models/account';
import { FRENCH_LABEL } from 'src/app/shared/settings/Constants';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-account-choice-ui-simple-step-two',
  templateUrl: './account-choice-ui-simple-step-two.component.html',
  styleUrls: ['./account-choice-ui-simple-step-two.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AccountChoiceUiSimpleStepTwoComponent
  implements OnInit, OnDestroy {
  selectedCountryCode = 'eu';
  countryCodes = ['eu','us','gb','ca','dz','bh','dk','le','jp','kw','ly','mr','ma','no','qa','sa','se','ch','ae'];
  accounts!: Account[];
  accounts_checked: Account[]=[];
  lang?= FRENCH_LABEL;

  constructor( public translate: TranslateService,
    private router: Router,
    public globalStateService: GlobalStateService,
    private accountservice: AccountService
  ) {}

  ngOnInit(): void {
    this.getAllAccounts();
   this.setLang()
  }

  accountSimpleChoiceToNextStep() {
    this.router.navigate(['offer/step3']);
    this.globalStateService.userChoice.personalInfo.activated =
      'active new_style';
    this.globalStateService.userChoice.progressBarValue = 25;
    this.globalStateService.userChoice.accounts = this.accounts_checked;
  }

  accountSimpleChoiceToPrevStep() {
    this.router.navigate(['offer']);
    this.globalStateService.userChoice.personalInfo.activated = '';
    this.globalStateService.userChoice.offer.activated = 'active new_style';
    this.globalStateService.userChoice.progressBarValue = 10;
  }

  getAllAccounts() {
    return this.accountservice.getAllAccounts().subscribe(
      (data) => {
        this.accounts = data;
      },
      (err: any) => console.log('HTTP Error', err),
      () => {
        this.accounts.map((x) => (x.checked = false));
      }
    );
  }

  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;
    console.log(this.selectedCountryCode)
  }

  ngOnDestroy(): void {
    this.globalStateService.userChoice.offer.activated = 'active';
  }

  setAllAccounts(account_cheched: Account) {
    this.accounts_checked.push(account_cheched)
    this.accounts_checked = this.accounts_checked.filter(x=>x.checked===true);
    console.log(this.accounts_checked);
  }
  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.translate.onLangChange.subscribe((data: any) => {
      console.log('onLangChange', data.lang);
      this.lang = data.lang;
      // temporairemnt
    });
  }
  setHeader(): void{

  }
}

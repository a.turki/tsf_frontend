import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountChoiceUiSimpleStepTwoComponent } from './account-choice-ui-simple-step-two.component';

describe('AccountChoiceUiSimpleStepTwoComponent', () => {
  let component: AccountChoiceUiSimpleStepTwoComponent;
  let fixture: ComponentFixture<AccountChoiceUiSimpleStepTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountChoiceUiSimpleStepTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountChoiceUiSimpleStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

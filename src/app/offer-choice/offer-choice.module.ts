import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfferChoiceUiComponent } from './offer-choice-ui/offer-choice-ui.component';
import { OfferChoiceUiStepOneComponent } from './offer-choice-ui-step-one/offer-choice-ui-step-one.component';
import { CivilStatusComponent } from './personal-info/civil-status/civil-status.component';
import { CheckMailComponent } from './personal-info/check-mail/check-mail.component';

import { AppRoutingModule } from '../app-routing.module';
import { MaterialModule } from '../material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AccountChoiceUiStepTwoComponent } from './account-choice-ui-step-two/account-choice-ui-step-two.component';
import { FormsModule } from '@angular/forms';
import { PersonalInfoUiStepThreeComponent } from './personal-info-ui-step-three/personal-info-ui-step-three.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { AccountChoiceUiSimpleStepTwoComponent } from './account-choice-ui-simple-step-two/account-choice-ui-simple-step-two.component';
import { AddressInfoComponent } from './personal-info/address-info/address-info.component';
import { FinancialInfoComponent } from './personal-info/financial-info/financial-info.component';
import { AgencyComponent } from './personal-info/agency/agency.component';
import { NgxFlagPickerModule } from 'ngx-flag-picker';
import { IntlTelInputNgModule } from 'intl-tel-input-ng';
import { FatcaDialogComponent } from './personal-info/civil-status/fatca-dialog/fatca-dialog.component';
import { RecapComponent } from './personal-info/recap/recap.component';

import { OfferChoiceUpdateComponent } from './personal-info/offer-choice-update/offer-choice-update.component';
import { CivilityStepUpdateComponent } from './personal-info/civility-step-update/civility-step-update.component';
import { CivilStatusUpdateComponent } from './personal-info/civil-status-update/civil-status-update.component';
import { AddressInfoUpdateComponent } from './personal-info/address-info-update/address-info-update.component';
import { PersonalInfoUpdateComponent } from './personal-info/personal-info-update/personal-info-update.component';
import { AgencyInfoUpdateComponent } from './personal-info/agency-info-update/agency-info-update.component';
import { SimpleAccountUpdateComponent } from './personal-info/simple-account-update/simple-account-update.component';
import { AccountOfferUpdateComponent } from './personal-info/account-offer-update/account-offer-update.component';
import { TestAccordiantComponent } from './personal-info/test-accordiant/test-accordiant.component';
import { JustificatifComponent } from './personal-info/justificatif/justificatif.component';
import { DowanloadFileComponent } from './personal-info/dowanload-file/dowanload-file.component';
import { DowanloadFileDoubleColumnComponent } from './personal-info/dowanload-file-double-column/dowanload-file-double-column.component';
import { ValidationMailComponent } from './personal-info/validation-mail/validation-mail.component';
import { CompleteRequestComponent } from './complete-request/complete-request.component';
import { SignatureContratComponent } from './signature-contrat/signature-contrat.component';
import { PreouvertureCompteComponent } from './preouverture-compte/preouverture-compte.component';
import { CompteValidationComponent } from './compte-validation/compte-validation.component';
import { DemandSuccessComponent } from './demand-success/demand-success.component';
import { MakeAppointmentComponent } from './make-appointment/make-appointment.component';
import { VisioMeetingComponent } from './visio-meeting/visio-meeting.component';
import { ContractComponent } from './contract/contract.component';

import { LegalNoticesComponent } from './personal-info-ui-step-three/legal-notices/legal-notices.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {JoyrideModule} from 'ngx-joyride';
import { JustificatifsComponent } from './personal-info/justificatifs/justificatifs.component';
import {NgxDropzoneModule} from 'ngx-dropzone';
import { TooltipModule } from 'ng2-tooltip-directive';


@NgModule({
  declarations: [
    OfferChoiceUiComponent,
    OfferChoiceUiStepOneComponent,
    AccountChoiceUiStepTwoComponent,
    PersonalInfoUiStepThreeComponent,
    AccountChoiceUiSimpleStepTwoComponent,
    CivilStatusComponent,
    CheckMailComponent,
    AddressInfoComponent,
    FinancialInfoComponent,
    AgencyComponent,
    FatcaDialogComponent,
    RecapComponent,
    JustificatifComponent,
    OfferChoiceUpdateComponent,
    CivilityStepUpdateComponent,
    CivilStatusUpdateComponent,
    AddressInfoUpdateComponent,
    PersonalInfoUpdateComponent,
    AgencyInfoUpdateComponent,
    SimpleAccountUpdateComponent,
    AccountOfferUpdateComponent,
    TestAccordiantComponent,
    JustificatifComponent,
    DowanloadFileComponent,
    DowanloadFileDoubleColumnComponent,
    ValidationMailComponent,
    CompleteRequestComponent,
    DowanloadFileComponent,
    DowanloadFileDoubleColumnComponent,
    SignatureContratComponent,
    PreouvertureCompteComponent,
    CompteValidationComponent,
    DemandSuccessComponent,
    LegalNoticesComponent,
    MakeAppointmentComponent,
    VisioMeetingComponent,
    ContractComponent,
    JustificatifsComponent,


  ],
  imports: [TooltipModule,NgxDropzoneModule, IntlTelInputNgModule.forRoot(), CommonModule, PdfViewerModule, MaterialModule, FlexLayoutModule, AppRoutingModule, FormsModule, ReactiveFormsModule, SharedModule, NgxFlagPickerModule, JoyrideModule],
})
export class OfferChoiceModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreouvertureCompteComponent } from './preouverture-compte.component';

describe('PreouvertureCompteComponent', () => {
  let component: PreouvertureCompteComponent;
  let fixture: ComponentFixture<PreouvertureCompteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreouvertureCompteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreouvertureCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

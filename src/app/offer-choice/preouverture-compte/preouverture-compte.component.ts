import { Component, OnInit } from '@angular/core';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';

@Component({
  selector: 'app-preouverture-compte',
  templateUrl: './preouverture-compte.component.html',
  styleUrls: ['./preouverture-compte.component.scss']
})
export class PreouvertureCompteComponent implements OnInit {

  constructor(public gsService: GlobalStateService) { }

  ngOnInit(): void {
    this.setHeader()
  }
  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 85;
    this.gsService.userChoice._step = 'signatureContrat';
  }
}

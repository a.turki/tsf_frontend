import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { AuthentificationToSign } from 'src/app/shared/models/authentificationToSign';
import { PersonalInfo } from 'src/app/shared/models/personalInfo';
import { SubscriberSignStatus } from 'src/app/shared/models/subscriberSignStatus';
import { ls } from 'src/app/shared/settings/AppSettings';

@Component({
  selector: 'app-demand-success',
  templateUrl: './demand-success.component.html',
  styleUrls: ['./demand-success.component.scss']
})
export class DemandSuccessComponent implements OnInit {
  persoInfoId  = ls.get("idPersonalInfo");
  
  personalInfo: PersonalInfo = new PersonalInfo();
  email: any = '';
  token: any = '';
  hasCertificate: any = '';
  status: any = '';
  
  constructor(public gsService: GlobalStateService, private router: Router, private personalInfoService: PersonalInfoService) { }

  ngOnInit(): void {
    this.setHeader();
   // console.log("pppppp"+Number(atob(this.persoInfoId)))
        this.getPersonalInfo(Number(this.persoInfoId));
    this.getSubsciberSatus(Number(this.persoInfoId));

  }

  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 65;
    this.gsService.userChoice._step = 'signatureContrat';
  }
 /* onSubmit() {

    this.router.navigate(['offer/contract']);
  }*/

  onSubmit() {
    this.personalInfoService.saveTokenToSign(this.email).subscribe((resp:AuthentificationToSign) => {
      this.token= resp.token
      console.log(resp.token)
    }, (err) => {
    });
    if ( (this.hasCertificate == 'false' )&&(this.status == "ND")){
    window.open('https://sign.attijaribank.com.tn/pub/registration/internalProfile?token=' + this.token + '&email=' + this.email, "_blank");
    }
    else if((this.hasCertificate == 'true' )&&(this.status == "READY")){
      this.router.navigate(['offer/signatureContrat'])
    }
    else if((this.hasCertificate == 'false' )&&(this.status == "EN_COURS")){
     // this.router.navigate(['offer/signatureContrat'])
    }
  }
  getPersonalInfo(id: number) {
    this.personalInfoService.findByPersonalId(id).subscribe(result => {
      this.personalInfo = result;
      this.email = this.personalInfo.email;
      console.log(this.personalInfo.nbrkids)
    })
  }


  getSubsciberSatus(id: number) {
    this.personalInfoService.getSubsciberSatus(id).subscribe((result:SubscriberSignStatus) => {
      this.hasCertificate =  result.hasCertificate;
      this.status =  result.status;

    })
  }
}

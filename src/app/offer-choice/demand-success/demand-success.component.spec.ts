import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandSuccessComponent } from './demand-success.component';

describe('DemandSuccessComponent', () => {
  let component: DemandSuccessComponent;
  let fixture: ComponentFixture<DemandSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemandSuccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

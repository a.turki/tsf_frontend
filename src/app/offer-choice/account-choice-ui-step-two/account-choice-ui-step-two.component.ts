import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account/account.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { Account } from 'src/app/shared/models/account';
import { FRENCH_LABEL } from 'src/app/shared/settings/Constants';
import { TranslateService } from '@ngx-translate/core';
import { Offer } from 'src/app/shared/models/offer';
import { LegalNoticesComponent } from '../personal-info-ui-step-three/legal-notices/legal-notices.component';
import { MatDialog } from '@angular/material/dialog';
import { ResponsiveTooltipComponent } from '../../shared/components/responsive-tooltip/responsive-tooltip.component';

@Component({
  selector: 'app-account-choice-ui-step-two',
  templateUrl: './account-choice-ui-step-two.component.html',
  styleUrls: ['./account-choice-ui-step-two.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AccountChoiceUiStepTwoComponent implements OnInit, OnDestroy {
  favoriteSeason!: string;
  seasons: any[] = [{ text: 'Winter', checked: true }, { text: 'Winter', checked: false }, { text: 'Winter', checked: true }];
  accounts!: Account[];
  selectedCountryCode = 'eu';
  countryCodes = ['eu', 'us', 'gb', 'ca', 'dz', 'bh', 'dk', 'le', 'jp', 'kw', 'ly', 'mr', 'ma', 'no', 'qa', 'sa', 'se', 'ch', 'ae'];
  accounts_checked: Account[] = [];
  lang?= FRENCH_LABEL;
  OfferName?: string;
  OfferPrice?: number;
  OfferUrl?: string;
  myOptions = {
    'placement': 'right',
    'show-delay': 500,
    'hide-delay': 300,
    'max-width': 200,
    'width': 200,
    'animationDuration': 300,
    'animationDurationDefault': 300
  }
  constructor(public translate: TranslateService, public dialog: MatDialog
    , private router: Router, public globalStateService: GlobalStateService, private accountservice: AccountService) {
    this.OfferName = this.globalStateService.userChoice.offer.name;
    this.OfferPrice = this.globalStateService.userChoice.offer.price;
    this.OfferUrl = "../../../assets/img/" + this.globalStateService.userChoice.offer.url;
    console.log(this.OfferUrl)
  }


  ngOnInit(): void {
    this.getAllAccounts();
    this.setLang();
    this.setHeader();
  }

  accountChoiceToNextStep() {
    this.globalStateService.userChoice._step = 'information';
    this.router.navigate(['offer/step3']);
    this.globalStateService.userChoice.personalInfo.activated = 'active new_style';
    this.globalStateService.userChoice.progressBarValue = 25;
    this.globalStateService.userChoice.accounts = this.accounts_checked;
  }
  accountChoiceToPrevStep() {
    this.globalStateService.userChoice._step = 'offer_choice';

    this.router.navigate(['offer']);
    this.globalStateService.userChoice.personalInfo.activated = "";
    this.globalStateService.userChoice.offer.activated = "active new_style";
    this.globalStateService.userChoice.progressBarValue = 10;
  }

  getAllAccounts() {

    return this.accountservice.getAllAccounts().subscribe(data => {
      this.accounts = data

    }, (err: any) => console.log('HTTP Error', err),
      () => {
        this.accounts.map(x => x.id === 1 ? x.checked = true : x.checked = false);
        this.accounts_checked.push(this.accounts[0]);
      })
  }

  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;
  }


  setAllAccounts(account_cheched: Account) {
    this.accounts_checked.push(account_cheched)
    this.accounts_checked = this.accounts_checked.filter(x => x.checked === true);
    console.log(this.accounts_checked);
  }

  ngOnDestroy(): void {
  }
  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.translate.onLangChange.subscribe((data: any) => {
      console.log('onLangChange', data.lang);
      this.lang = data.lang;
      // temporairemnt
    });
  }
  setHeader(): void {
    this.globalStateService.userChoice.offer.activated = 'active new_style';
    this.globalStateService.userChoice._step = 'target_account';
    this.globalStateService.userChoice.progressBarValue = 10;

  }

  triggerTooltip(): void {
    const dialogRef = this.dialog.open(ResponsiveTooltipComponent, {
      height: '220px',
      width: '100%',
      maxWidth: '93%',
      position: { bottom: '0px' }
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
    });
  }
}

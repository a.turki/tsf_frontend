import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountChoiceUiStepTwoComponent } from './account-choice-ui-step-two.component';

describe('AccountChoiceUiStepTwoComponent', () => {
  let component: AccountChoiceUiStepTwoComponent;
  let fixture: ComponentFixture<AccountChoiceUiStepTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountChoiceUiStepTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountChoiceUiStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { PersonalInfo } from 'src/app/shared/models/personalInfo';

import { UserInfoChoice } from 'src/app/shared/types/UserInfoChoice';
import { COUNTRIES_CODE } from 'src/app/shared/settings/Constants';
import { IntlTelInputComponent } from 'intl-tel-input-ng';
import { FatcaDialogComponent } from '../personal-info/civil-status/fatca-dialog/fatca-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { LegalNoticesComponent } from './legal-notices/legal-notices.component';

export interface NativeCountry {
  name: string;
  flag: string;
}

@Component({
  selector: 'app-personal-info-ui-step-three',
  templateUrl: './personal-info-ui-step-three.component.html',
  styleUrls: ['./personal-info-ui-step-three.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PersonalInfoUiStepThreeComponent implements OnInit, OnDestroy {
  @ViewChild(IntlTelInputComponent)
  phoneNumberComp!: IntlTelInputComponent;
  countryCodes = COUNTRIES_CODE;
  personalInfoForm!: FormGroup;
  myOptions = {
    'placement': 'right',
    'show-delay': 500,
    'hide-delay': 300,
    'max-width': 200,
    'width': 200,
    'animationDuration': 300,
    'animationDurationDefault': 300
  }
  phoneForm = new FormGroup({
    phone: new FormControl(undefined, [Validators.required])
  });
  selectedCountryCode = 'us';
  NATIVE_COUNTRY_FLAG_FR_PATH = '../../../../assets/flags/nativeCountriesFR-test.json';
  NATIVE_COUNTRY_FLAG_EN_PATH = '../../../../assets/flags/nativeCountriesEN-test.json';
  selectedNativeCountry: NativeCountry = { name: 'Françe', flag: 'fr' };
  nativeCountries_fr: NativeCountry[] = [];
  nativeCountries_en: NativeCountry[] = [];
  nativeCountries: NativeCountry[] = [];
  submitted = false;
  E164PhoneNumber = '';
  userChoiceInfo = new UserInfoChoice;
  isMailInputFocused = false;
  isPhoneInputFocused = false;
  private continue = false;
  constructor(private http: HttpClient,
    public globalStateService: GlobalStateService,
    private formBuilder: FormBuilder,
    private personalInfoService: PersonalInfoService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {

    this.personalInfoForm = this.formBuilder.group({
      civility: ['', Validators.required],
      firstName: ['', [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z ]*$')]],
      lastName: ['', [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z ]*$')]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      nativeCountry: ['', Validators.required],
      birthday: ['', Validators.required]
    });


    /*** we need both of them to set our variables */
    forkJoin([
      this.getJSON(this.NATIVE_COUNTRY_FLAG_FR_PATH),
      this.getJSON(this.NATIVE_COUNTRY_FLAG_EN_PATH)]).subscribe(t => {
        this.nativeCountries_fr = t[0];
        this.nativeCountries = t[0];
        this.nativeCountries_en = t[1];
        this.setValues();
      });
    this.setHeader();
  }


  setValues() {
    this.personalInfoForm.patchValue({
      nativeCountry: this.nativeCountries.find(x => x.flag === 'TN')!.name,
      birthday: new Date()
    });
  }


  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;
  }

  focusMailInput() {
    this.isMailInputFocused = true;
  }
  focusPhoneInput() {
    this.isPhoneInputFocused = true;
    console.log(this.isPhoneInputFocused);

  }
  numberOnly(event: any): boolean {
    this.isPhoneInputFocused = false;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  ngOnDestroy(): void {
    //  this.globalStateService.userChoice.personalInfo.activated=false;
  }


  personalInfoToNextStep() {
    this.submitted = true;

    this.personalInfoForm.get('phone')?.setValue(this.E164PhoneNumber);


    this.userChoiceInfo.civility = this.personalInfoForm.get('civility')?.value;
    this.userChoiceInfo.firstName = this.personalInfoForm.get('firstName')?.value;
    this.userChoiceInfo.lastName = this.personalInfoForm.get('lastName')?.value;
    this.userChoiceInfo.email = this.personalInfoForm.get('email')?.value;
    this.userChoiceInfo.phone = this.personalInfoForm.get('phone')?.value;
    this.userChoiceInfo.birthday = this.personalInfoForm.get('birthday')?.value;
    this.userChoiceInfo.langue = 'fr';
    this.userChoiceInfo.nativeCountry = this.personalInfoForm.get('nativeCountry')?.value;
    this.userChoiceInfo.accounts = this.globalStateService.userChoice.accounts;

    if (this.personalInfoForm.invalid) {
      return;
    }
    console.log('firsname' + this.userChoiceInfo.firstName);
    this.personalInfoService.save(this.userChoiceInfo).subscribe((data: PersonalInfo) => {
      console.log('personalInfo', data);
      sessionStorage.setItem('pi', JSON.stringify(data));
      sessionStorage.setItem('userChoiceInfo', JSON.stringify(this.userChoiceInfo));
    });
    console.log(this.personalInfoForm.getRawValue());
    this.router.navigate(['offer/check-mail']);
    this.globalStateService.userChoice.personalInfo.activated = 'active new_style';
    this.globalStateService.userChoice.offer.activated = 'active';
    this.globalStateService.userChoice.progressBarValue = 25;
  }

  personalInfoToPrevStep() {
    this.globalStateService.userChoice._step = 'target_account';

    this.router.navigate(['offer/step2']);
    this.globalStateService.userChoice.personalInfo.activated = '';
    this.globalStateService.userChoice.offer.activated = 'active new_style';
    this.globalStateService.userChoice.progressBarValue = 10;
  }

  public getJSON(path: string): Observable<any> {
    return this.http.get(path);
  }


  getSelectedFlag() {

    const obj = this.nativeCountries.find(x => x.name === this.personalInfoForm.get('nativeCountry')!.value);

    return { name: obj?.name, flag: obj?.flag };
  }


  get f() { return this.personalInfoForm.controls; }

  // tslint:disable-next-line:typedef
  validation(form: NgForm) {
    if (form.invalid) {
      document.getElementsByName('phone')[0].style.border = 'solid 1px #f44336';
    }
    else {
      document.getElementsByName('phone')[0].style.border = 'solid 1px #d6d6d6';
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(LegalNoticesComponent);

    dialogRef.afterClosed().subscribe((result: boolean) => {
      this.continue = result;
    });
  }

  setHeader(): void {
    this.globalStateService.userChoice.personalInfo.activated = 'active new_style';
    this.globalStateService.userChoice.offer.activated = 'active';
    this.globalStateService.userChoice.progressBarValue = 22;
    this.globalStateService.userChoice._step = 'information';
  }
}

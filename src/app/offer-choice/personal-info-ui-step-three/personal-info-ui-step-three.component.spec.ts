import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalInfoUiStepThreeComponent } from './personal-info-ui-step-three.component';

describe('PersonalInfoUiStepThreeComponent', () => {
  let component: PersonalInfoUiStepThreeComponent;
  let fixture: ComponentFixture<PersonalInfoUiStepThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalInfoUiStepThreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalInfoUiStepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-legal-notices',
  templateUrl: './legal-notices.component.html',
  styleUrls: ['./legal-notices.component.scss']
})
export class LegalNoticesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

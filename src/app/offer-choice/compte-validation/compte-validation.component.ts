import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';

@Component({
  selector: 'app-compte-validation',
  templateUrl: './compte-validation.component.html',
  styleUrls: ['./compte-validation.component.scss']
})
export class CompteValidationComponent implements OnInit {

  constructor(public gsService: GlobalStateService,private router: Router) { }

  ngOnInit(): void {
    this.setHeader();
  }

  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 100;
    this.gsService.userChoice._step = 'validationCompte';
  }
  onSubmit()
{
  
 
  this.router.navigate(['offer/validationCompte']);

 

}
}

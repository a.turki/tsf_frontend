import { OnDestroy } from '@angular/core';

import { MediaChange,MediaObserver } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

import { HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { PersonalInfo } from 'src/app/shared/models/personalInfo';

import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { FormArray } from '@angular/forms';


import { ResidencyDocument } from 'src/app/shared/models/residencyDocument';
import { ls } from 'src/app/shared/settings/AppSettings';

import { RequiredDoc } from 'src/app/shared/models/requiredDoc';
import { RequiredDocResidency } from 'src/app/shared/models/requiredDocResidency';
import { JustifRevenu } from 'src/app/shared/models/justifRevenu';
import { FileData } from 'src/app/shared/models/fileData';
import { FRENCH_LABEL } from 'src/app/shared/settings/Constants';
import { RequiredDocIncome } from 'src/app/shared/models/requiredDocIncome';
import { OtherRevenuFile } from 'src/app/shared/models/OtherRevenuFile';
import { OtherResidencyFile } from 'src/app/shared/models/otherResidencyFile';
import { Input } from '@angular/core';
import { ViewChild } from '@angular/core';
import { JustificatifsService } from 'src/app/services/justificatifs.service';




export interface NativeCountry {


  name: string;
  flag: string;
}
@Component({
  selector: 'app-justificatif',
  templateUrl: './justificatif.component.html',
  styleUrls: ['./justificatif.component.scss']
})
export class JustificatifComponent implements OnInit, OnDestroy {
  @Input()
  maxlength: string | number | undefined;
  maxSize = 5 * 1024 * 1024
  @ViewChild('ref') ref:any;
  lang?= FRENCH_LABEL;
  justificatifsForm!: FormGroup;
  checked: boolean = true;
  justificatifsFormValues!: FormGroup;
  isJustifiResidenceInputFocused=false;
  selectedFiles?: FileList;
  currentFile?: File;
  indexAmrican?:boolean;
  residencyDocumentList?:ResidencyDocument[];
  revenuListDocumentList?:RequiredDocIncome[];
  revenuListOtherRevenuFileList?:OtherRevenuFile[];
  revenuListOtherResidencyFileList?:OtherResidencyFile[];
  revenuList : FileData[] =[];
  residencyList : FileData[] = [];
  nationalite= new RequiredDocIncome();
  allTypeJustifRevenuList?:JustifRevenu[];
  fileData= new FileData();
  listFileAutherRisdency?:FileData[]=[];
  listFileAutherRevenu?:FileData[]=[];
  requestId=1;
  personalInfo=new PersonalInfo;
  fileName?:String;
  verficationUploadCinRecto?:boolean=true;
  verficationUploadCinVerso?:boolean=true;
  verficationUploadNationalite?:boolean=true;
  verficationUploadRevenu?:boolean=true;
  verficationUploadResidencyRecto?:boolean=true;
  verficationUploadResidencyVerso?:boolean=true;
  verficationUploadFatca?:boolean=true;
  verficationUploadAuther?:boolean=true;
  verficationUploaNationalite?:boolean=true;

  isDisabled=false;
  submit = false;
  illimitedDate?=false;
  nameFileRecto?:any;
  nameFileVerso?:any;
  nameFileResidencyRecto?:any;
  nameFileResidencyverso?:any;
  nameFileFatca?:any;
  nameFileRevenu?:String;
  nameFileAtherRisdency?:String;
  nameFileNationalite?:any;
  requiredDoc=new RequiredDoc;
  reqId = ls.get("reqId");
  otherRevenuFile=new OtherRevenuFile();
  progress :boolean=false;
  progress2 :boolean=false;
  progressFile?:any;
  file: any;
  dateAujourdhuit=new Date();
  justifId!:number;
  residencyId!:number;
  sizeFileMax=false;


  i?:number;
  requiredDocResidency= new RequiredDocResidency;
  constructor( private formBuilder: FormBuilder,
               private justificatifsService:JustificatifsService,
               private personalInfoService:PersonalInfoService,
               private changeDetectorRef:ChangeDetectorRef,
               public gsService: GlobalStateService,
               private router: Router) { }

  ngOnInit(): void {
    this.setHeader();
    this.revenuList.push(this.fileData);
    this.justificatifsForm = this.formBuilder.group({
      identNationale: ['', Validators.required],
      numCarteIdentite:['',Validators.required,Validators.pattern("^[0-9]*$"),Validators.minLength(8)],
      dateDelivranceCarteIdentite:['',Validators.required],
      residencyDocumentId:['',Validators.required],
      justifRevenuId:['',Validators.required],
      numDocumentJustif:['',Validators.required,Validators.pattern("^[0-9]*$")],
      deliveryDate:['',Validators.required],
      experationDate:[{value: '', disabled: this.isDisabled}],
      illimitedExperationDate:['']
    })

    this.justificatifsService.getJustifById(Number (this.reqId)).subscribe((res:any)=>{
      this.justificatifsForm.get('numCarteIdentite')?.setValue(res.numCarteIdentite);
      this.justificatifsForm.get('dateDelivranceCarteIdentite')?.setValue(res.dateDelivranceCarteIdentite);
      this.justificatifsForm.get('deliveryDate')?.setValue(res.deliveryDate);
      this.justificatifsForm.get('residencyDocumentId')?.setValue(res.numCarteIdentite);
      this.justificatifsForm.get('numDocumentJustif')?.setValue(res.numDocumentJustif);
      this.justificatifsForm.get('experationDate')?.setValue(res.experationDate);
      this.justificatifsForm.get('illimitedExperationDate')?.setValue(res.illimitedExperationDate);
      if(res.fatca!=null){
        let x =res.fatca.split("/")
        this.nameFileFatca=x[2];
        this.verficationUploadFatca=false;

      }
      if(res.rectoCin!=null){
        let x =res.rectoCin.split("/")
        this.nameFileRecto=x[2];
        this.verficationUploadCinRecto=false ;

      }
      if(res.versoCin!=null){
        let x =res.versoCin.split("/")
        this.nameFileVerso=x[2];
        this.verficationUploadCinVerso=false;

      }
      if(res.residencyRecto!=null){
        let x =res.residencyRecto.split("/")
        this.nameFileResidencyRecto=x[2];
        this.verficationUploadResidencyRecto=false;

      }
      if(res.residencyVerso!=null){
        let x =res.residencyVerso.split("/")
        this.nameFileResidencyverso=x[2];
        this.verficationUploadResidencyVerso=false;

      }

      console.log("le valeur est ",res)



    },error=>{
      console.error;
    });
    this.justificatifsService.getAllRevenuDocumentById(Number (this.reqId)).subscribe(res=>{
      this.revenuListOtherRevenuFileList=res;

    },error=>{
      console.error;
    });
    this.justificatifsService.getAllResidencyDocumentById(Number (this.reqId)).subscribe(res=>{
      this.revenuListOtherResidencyFileList=res;

    },error=>{
      console.error;
    });
    this.justificatifsService.getAllResidencyDocument().subscribe(res=>{
      this.residencyDocumentList=res;


    },error=>{
      console.error;
    });
    this.justificatifsService.getAllJustifRevenu().subscribe(res=>{
      this.allTypeJustifRevenuList=res;

    },error=>{
      console.error;
    });
    this.justificatifsService.getNasionaliteByRequestId(Number (this.reqId)).subscribe(res=>{
      this.nationalite=res;
      if( this.nationalite.path!=null){
        let x =this.nationalite.path.split("/")
        this.nameFileNationalite=x[2];
        this.verficationUploaNationalite=false;

      }


    },error=>{
      console.error;
    });
    this.personalInfoService.findByRequestId(Number (this.reqId)).subscribe(res=>{
      this.personalInfo=res;
      this.indexAmrican=this.personalInfo.americanIndex;
      this.changeDetectorRef.detectChanges();

    })
    this.setLang();
  }
  get numCarteIdentite() {
    return this.justificatifsForm.get('numCarteIdentite');
  }
  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;}
  get f() { return this.justificatifsForm.controls; }
  get justifResidenceList()
  {
    return this.justificatifsForm.get('justifResidenceList') as FormArray;
  }
  get justifRevenuList()
  {
    return this.justificatifsForm.get('justifRevenuList') as FormArray;
  }

  showOptions(value:any){
    console.log("le connt de value avant ",value)
    this.checked = !value;
    if(value){
      this.justificatifsForm.controls['experationDate'].disable();

    }
    else {
      this.justificatifsForm.controls['experationDate'].enable();
    }


  }
  onSubmit()
  {
    console.log("le contnu de form",this.justificatifsForm.value)
    this.justificatifsService.saveRequiredDoc(this.justificatifsForm.value,this.reqId).subscribe
    (res=>
      {
        console.log("le conteu de res est",res);
      }
    )
    this.router.navigate(['offer/demandSuccess']);
  }

  uploadFileRevenu(event: any,i:number){
    this.selectedFiles = event.target.files;
    this.selectedFiles = event.target.files;
    this.progress=true;
    if (this.selectedFiles){
      const file: File | null = this.selectedFiles.item(0);

      if (file)
      {
        this.currentFile = file;
        this.justificatifsService.uploadFileRevenu(this.currentFile,this.reqId).subscribe((res:any)=>{
          this.fileData=res.body;
          setTimeout(()=>{
            let element=(<HTMLInputElement>document.getElementById("fileDataRevenue"+i));
            element.style.display="none";
            if(this.fileData.enable!=undefined){
              this.progress=this.fileData.enable;
            }
          }, 3000);

          this.justificatifsService.getAllRevenuDocumentById(Number (this.reqId)).subscribe(res=>{
            this.revenuListOtherRevenuFileList=res;


          },error=>{
            console.error;
          });
        })





      }
    }



  }
  uploadFileResidency(event: any,i:number){
    this.selectedFiles = event.target.files;
    this.selectedFiles = event.target.files;

    this.progress2=true;
    if (this.selectedFiles){
      const file: File | null = this.selectedFiles.item(0);

      if (file)
      {
        this.currentFile = file;
        this.justificatifsService.uploadResidency(this.currentFile,this.reqId).subscribe((res:any)=>{
          if (event.type === HttpEventType.UploadProgress) {
            console.log("le contenu de fichie",res)


          }
          this.fileData=res.body;
          setTimeout(()=>{
            let element=(<HTMLInputElement>document.getElementById("fileDataResidency"+i));
            element.style.display="none";
            if(this.fileData.enable!=undefined){
              this.progress2=this.fileData.enable;
            }
          }, 3000);

          this.justificatifsService.getAllResidencyDocumentById(Number (this.reqId)).subscribe(res=>{
            this.revenuListOtherResidencyFileList=res;

          },error=>{
            console.error;
          });
        })


      }
    }



  }
  get j(){
    return this.justificatifsForm.controls
  }
  uploadFile(event: any,typeDoc:String) {
    this.progressFile=true;
    this.selectedFiles = event.target.files;
    if (this.selectedFiles)
    {
      const file: File | null = this.selectedFiles.item(0);
      if (file) {
        this.currentFile = file;
        console.log("size de file est",this.currentFile.size)

        this.justificatifsService.uploadFile(this.currentFile,this.reqId,typeDoc).subscribe(
          (event: any) => {

            this.fileData=event.body;
            this.fileName=file.name;
            switch(typeDoc)
            {
              case "rectoFile": {
                this.nameFileRecto=file.name;
                this.verficationUploadCinRecto=false ;

                break;
              }
              case "versoFile":
              {
                this.nameFileVerso=file.name;
                this.verficationUploadCinVerso=false;
                break;
              }
              case "fitcaFile":
              {
                this.nameFileFatca=file.name;
                this.verficationUploadFatca=false;
                break;
              }
              case "residencyRecto":
              {
                this.nameFileResidencyRecto=file.name;
                this.verficationUploadResidencyRecto=false;
                break;
              }
              case "residencyVerso":
              {
                this.nameFileResidencyverso=file.name;
                this.verficationUploadResidencyVerso=false;
                break;
              }
              case "nationaliteFile":
              {
                this.nameFileNationalite=file.name;
                this.verficationUploaNationalite=false;
                break;
              }
              case "pieceRevenuFile":
              {
                this.nameFileRevenu=file.name;
                this.verficationUploadRevenu=false;

              }

                break;
            }
          })
      }

    }

  }

  deleteFile(typeDoc:String) {
    this.justificatifsService.deleteFile(Number (this.reqId),typeDoc).subscribe(
      (res) => {console.log("save file",typeDoc);
        console.log("save filddddde",res)
        switch(typeDoc) {
          case "rectoFile": {
            this.verficationUploadCinRecto=true;
            break;
          }
          case "versoFile": {

            this.verficationUploadCinVerso=true;
            break;
          }
          case "fitcaFile": {

            this.verficationUploadFatca=true;
            break;
          }
          case "residencyRecto": {

            this.verficationUploadResidencyRecto=true;
            break;
          }
          case "residencyVerso": {

            this.verficationUploadResidencyVerso=true;
            break;
          }
          case "nationaliteFile": {

            this.verficationUploaNationalite=true;
            break;
          }
          case "pieceRevenuFile": {
            this.verficationUploadRevenu=true;

          }

            break;
        }

      })


  }







  focusJustifiResidenceInput(){
    this.isJustifiResidenceInputFocused =true;
  }



  ngOnDestroy(): void {

  }

  next() {
    if (this.justificatifsForm.invalid) {
      this.submit = true;
      return;
    }
  }
  addFormRevenu(){
    if (this.revenuList.length <3){
      this.fileData=new FileData();
      this.revenuList.push(this.fileData);
    }
  }

  addFormResidency(){
    if (this.residencyList.length < 3){
      this.fileData=new FileData();
      this.residencyList.push(this.fileData);
    }



  }
  deleteFiledRevenu(id:number,i:number){
    this.justificatifsService.deleteRevenu(id).subscribe(res=>{

      this.justificatifsService.getAllRevenuDocumentById(Number (this.reqId)).subscribe(res=>{
        this.revenuListOtherRevenuFileList=res;
        if( this.revenuListOtherRevenuFileList?.length==0){
          let element=(<HTMLInputElement>document.getElementById("fileDataRevenue"+i));
          element.style.display="block";

        }
      },error=>{
        console.error;
      });

    })

    this.revenuListOtherRevenuFileList?.splice(i);
  }
  deleteResidency(id:number,i:number){
    this.justificatifsService.deleteResidency(id).subscribe(res=>{

      this.justificatifsService.getAllResidencyDocumentById(Number (this.reqId)).subscribe(res=>{
        this.revenuListOtherResidencyFileList=res;
        if( this.revenuListOtherResidencyFileList==[]){
          let element=(<HTMLInputElement>document.getElementById("fileDataResidency"+i));
          element.style.display="block";

        }
      },error=>{
        console.error;
      });

    })

    this.revenuListOtherResidencyFileList?.splice(i);
  }
  async downloadFile(id:any)
  {
    await this.justificatifsService.downloadFile(this.reqId,id).toPromise().then(data =>
    {
      this.file =data ;
    });
    let url = window.URL.createObjectURL(this.file);
    let a = document.createElement('a');
    a.href = url;
    a.download = id;
    a.click();

    window.URL.revokeObjectURL(url);
  }
  async downloadFileFileRevenu(id:any)

  {
    console.log("le contenu de j est ",id)
    await this.justificatifsService.downloadFileRevnu(id).toPromise().then(data =>
    {
      this.file =data ;
    });
    let url = window.URL.createObjectURL(this.file);
    let a = document.createElement('a');
    a.href = url;
    a.download = id;
    a.click();

    window.URL.revokeObjectURL(url);
  }
  async downloadFileFileAutherResidency(id:any)
  {
    console.log("le contenu de i est ",id)
    await this.justificatifsService.downloadFileAutherResidency(id).toPromise().then(data =>
    {
      this.file =data ;
    });
    let url = window.URL.createObjectURL(this.file);
    let a = document.createElement('a');
    a.href = url;
    a.download = id;
    a.click();

    window.URL.revokeObjectURL(url);
  }

  async downloadFileFatca(id:any)

  {

    await this.justificatifsService.downloadFileFatca().toPromise().then(data =>
    {
      this.file =data ;
    });
    let url = window.URL.createObjectURL(this.file);
    let a = document.createElement('a');
    a.href = url;
    a.download = id;
    a.click();

    window.URL.revokeObjectURL(url);
  }
  getValueJustifRevenuId(event:any){
    this.justifId= event.value;
    console.log("le contenu de justif id est",this.justifId)
  }
  getValueResidencyId(event:any){
    this.residencyId= event.value;
    console.log("le contenu de justif id est",this.residencyId)
  }

  setHeader() {
    this.gsService.userChoice.justif = 'active new_style';
    this.gsService.userChoice.recup = 'active ';
    this.gsService.userChoice.offer.activated = 'active';
    this.gsService.userChoice.personalInfo.activated = 'active';
    this.gsService.userChoice.progressBarValue = 60;
    this.gsService.userChoice._step = 'justificatifs';
  }
}


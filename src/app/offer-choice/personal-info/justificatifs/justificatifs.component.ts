import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JustificatifsService } from '../../../services/justificatifs.service';

@Component({
  selector: 'app-justificatifs',
  templateUrl: './justificatifs.component.html',
  styleUrls: ['./justificatifs.component.scss']
})
export class JustificatifsComponent implements OnInit {
  file: any;
  reqId: any;
  abroadResidencyProof: any;
  incomeProof: any;
  allDisplayedIncome = false;
  allDisplayedResidency = false;
  abroadResidencyList =
    {
      firstProof: true,
      secondProof: true,
      thirdProof: true,
    };
  incomeProofList =
    {
      firstProof: true,
      secondProof: true,
      thirdProof: true,
    };
  constructor(
    private justificatifsService: JustificatifsService, private router: Router

  ) { }

  ngOnInit(): void {
    this.reqId = localStorage.getItem('reqId');
  }





  // tslint:disable-next-line:typedef
  async downloadFileFatca(id: any) {
    await this.justificatifsService.downloadFileFatca().toPromise().then(data => {
      this.file = data;
    });
    const url = window.URL.createObjectURL(this.file);
    const doc = document.createElement('a');
    doc.href = url;
    doc.download = id;
    doc.click();
    window.URL.revokeObjectURL(url);
  }
  showMoreProofDisplay(type: string): void {
    if (type === 'residency') {
      if (this.abroadResidencyList.firstProof) {
        this.abroadResidencyList.firstProof = false;
        this.hidePlusResidency();
        return;
      }
      if (this.abroadResidencyList.secondProof) {
        this.abroadResidencyList.secondProof = false;
        this.hidePlusResidency();
        return;
      }
      if (this.abroadResidencyList.thirdProof) {
        this.abroadResidencyList.thirdProof = false;
        this.hidePlusResidency();
        return;
      }
    } else {
      if (this.incomeProofList.firstProof) {
        this.incomeProofList.firstProof = false;
        this.hidePlusIncome();
        return;
      }
      if (this.incomeProofList.secondProof) {
        this.incomeProofList.secondProof = false;
        this.hidePlusIncome();
        return;
      }
      if (this.incomeProofList.thirdProof) {
        this.incomeProofList.thirdProof = false;
        this.hidePlusIncome();
        return;
      }
    }
  }
  hidePlusResidency(): void {
    if (!this.abroadResidencyList.firstProof) {
      if (!this.abroadResidencyList.secondProof) {
        if (!this.abroadResidencyList.thirdProof) {
          this.allDisplayedResidency = true;
        }
      }
    }
  }
  hidePlusIncome(): void {
    if (!this.incomeProofList.firstProof) {
      if (!this.incomeProofList.secondProof) {
        if (!this.incomeProofList.thirdProof) {
          this.allDisplayedIncome = true;
        }
      }
    }
  }
  hideProofDisplay(id: number, type: string): void {
    if (type === 'residency') {
      switch (id) {
        case 1: {
          this.abroadResidencyList.firstProof = true;
          this.allDisplayedResidency = false;
          break;
        }
        case 2: {
          this.abroadResidencyList.secondProof = true;
          this.allDisplayedResidency = false;
          break;
        }
        case 3: {
          this.abroadResidencyList.thirdProof = true;
          this.allDisplayedResidency = false;
          break;
        }
      }
    }
    else {
      switch (id) {
        case 1: {
          this.incomeProofList.firstProof = true;
          this.allDisplayedIncome = false;
          break;
        }
        case 2: {
          this.incomeProofList.secondProof = true;
          this.allDisplayedIncome = false;
          break;
        }
        case 3: {
          this.incomeProofList.thirdProof = true;
          this.allDisplayedIncome = false;
          break;
        }
      }
    }
  }

  onSubmit() {
    this.router.navigate(['offer/demandSuccess']);
  }
  retourRecap() {
    this.router.navigate(['offer/summary']);

  }
}

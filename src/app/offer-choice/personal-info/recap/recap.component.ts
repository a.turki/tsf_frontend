import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { OfferService } from 'src/app/services/offer/offer.service';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { Account } from 'src/app/shared/models/account';
import { AddressInfo } from 'src/app/shared/models/addressInfo';
import { Agency } from 'src/app/shared/models/agency';
import { FinancialInfo } from 'src/app/shared/models/financialInfo';
import { InformationFinancier } from 'src/app/shared/models/InformationFinancier';
import { Offer } from 'src/app/shared/models/offer';
import { RequestInfo } from 'src/app/shared/models/RequestInfo';
import { ls } from 'src/app/shared/settings/AppSettings';
import { FRENCH_LABEL } from 'src/app/shared/settings/Constants';
import { PersonalInfo } from '../../../shared/models/personalInfo';

@Component({
  selector: 'app-recap',
  templateUrl: './recap.component.html',
  styleUrls: ['./recap.component.scss']
})
export class RecapComponent implements OnInit {
  pi: PersonalInfo = new PersonalInfo();
  personalInfo: PersonalInfo = new PersonalInfo();
  addressInfo: AddressInfo = new AddressInfo();
  financialInfo: InformationFinancier = new InformationFinancier();
  addressInfoRecap: Agency = new Agency();
  offer: Offer = new Offer();
  reqId = ls.get('reqId');
  persoInfoId = ls.get('idPersonalInfo')  ;
  lang ?= FRENCH_LABEL;
  request: RequestInfo = new RequestInfo ();
  accounts: Account[] = [];

  constructor( public gsService: GlobalStateService, public translate: TranslateService, private route: ActivatedRoute, public router: Router, private offerService: OfferService, private personalInfoService: PersonalInfoService) { }

  ngOnInit(): void {
    this.setHeader();
    this.setModel();
    this.setLang();
    this.getRequest(Number(this.reqId));
    this.getOfferName(Number(this.reqId));
    this.getPersonalInfo(Number(this.persoInfoId));
    this.getAddress(Number(this.persoInfoId));
    this.findFinancialInfo(Number(this.persoInfoId));
    this.findAgency(Number(this.persoInfoId));
  }


  setModel() {
    if (sessionStorage.getItem('pi') !== null) {
      const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
      this.pi = session_pi;
      console.log('pi', this.pi);
    }

  }

  getOfferName(id: number){
   return this.offerService.getOfferByReqId(id).subscribe(data => {
      this.offer = data;
      console.log(this.offer);
    });
  }
  getPersonalInfo(id: number){
  this.personalInfoService.findByPersonalId(id).subscribe(result => {
  this.personalInfo = result;
  console.log(this.personalInfo.nbrkids);
});
  }
  getAddress(id: number){
    this.personalInfoService.findAddress(id).subscribe(result => {
      this.addressInfo = result;
    });
   }
   findFinancialInfo(id: number){
    this.personalInfoService.findFinancialInfo(id).subscribe(result => {
      this.financialInfo = result;
    });
   }

   findAgency(id: number){
    this.personalInfoService.findAgency(id).subscribe(result => {
      this.addressInfoRecap = result;
    });
   }
   setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.translate.onLangChange.subscribe((data: any) => {
      console.log('onLangChange', data.lang);
      this.lang = data.lang;
      // temporairemnt
    });
  }

  getRequest(id: number){
    this.personalInfoService.getRequest(id).subscribe((data: RequestInfo) => {
      this.request = data;
      this.accounts = this.request.bankAccounts !== undefined ? this.request.bankAccounts : [];
      console.log(this.accounts);
    });
  }


  modifyOffre() {
    this.router.navigate(['offer/OfferChoiceUpdate']);
  }

  modifyCivility() {
    this.router.navigate(['offer/CivilitystepUpdate']);
  }

  modifyCivilityStatus() {
    this.router.navigate(['offer/CivilStatusUpdate']);
  }

  modifyAddresse() {
    this.router.navigate(['offer/AddressInfoUpdate']);
  }

  modifyFinancialInfo() {
    this.router.navigate(['offer/FinancialInfoUpdate']);
  }

  modifyAgency() {
    this.router.navigate(['offer/AgencyInfoUpdate']);
  }

  prevStep() {
    this.router.navigate(['offer/agency-info']);
  }

  modifyAccount(){
    this.router.navigate(['offer/SimpleAccountUpdate']);
  }
  setHeader() {
    this.gsService.userChoice.recup = 'active new_style';
    this.gsService.userChoice.offer.activated = 'active';
    this.gsService.userChoice.personalInfo.activated = 'active';
    this.gsService.userChoice.progressBarValue = 46;
    this.gsService.userChoice._step = 'summary';
  }
  passToJustificatif(){
    this.router.navigate(['offer/justificatif']);
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FinancialInfo } from 'src/app/shared/models/financialInfo';
import { PersonalInfoService } from '../../../services/personalInfo/personal-info.service';
import { PersonalInfo } from '../../../shared/models/personalInfo';
import { TranslateService } from '@ngx-translate/core';
import { Category } from 'src/app/shared/models/category';
import { Activity } from 'src/app/shared/models/activity';
import { ls } from 'src/app/shared/settings/AppSettings';
import { FRENCH_LABEL } from 'src/app/shared/settings/Constants';
import { InformationFinancier } from 'src/app/shared/models/InformationFinancier';
import { MonthlyNetIncome } from 'src/app/shared/models/MonthlyNetIncome';

@Component({
  selector: 'app-personal-info-update',
  templateUrl: './personal-info-update.component.html',
  styleUrls: ['./personal-info-update.component.scss']
})
export class PersonalInfoUpdateComponent implements OnInit {
  lang?= FRENCH_LABEL;
  pi: PersonalInfo = new PersonalInfo();
  financialInfo: FinancialInfo = new FinancialInfo();

  category: Category = new Category();
  activity: Activity = new Activity();
  monthlyNetIncome: MonthlyNetIncome = new MonthlyNetIncome();
  categories: Category[] = [];
  activities: Activity[] = [];
  monthlyNetIncomes: MonthlyNetIncome[] = [];

  incomes_fr: string[] = ['Entre 1500 et 3000', 'Entre 3000 et 4500', 
  'Entre 4500 et 6000', 'Entre 6000 et 7500'];
  incomes_en: string[] = ['Between 1500 and 3000', 'Between 3000 and 4500', 
  'Between 4500 and 6000', 'Between 6000 and 7500'];
  persoInfoId =ls.get("idPersonalInfo") ;
  incomes : string[] = [];
  netIcome: string = '';
  informationFinancier: FinancialInfo = new FinancialInfo();
  /**
   * 
   * @param router personalInfoId: number, monthlyNetIncome: number, categoryId: number,
        activityId: number
   * @param personalInfoService 
   */

  constructor(public router: Router, private personalInfoService: PersonalInfoService,
    public translate: TranslateService) { }

  ngOnInit(): void {
    /** recuperer le modele depuis la session si il existe */
    this.setModel();
    this.findFinancialInfo(Number(this.persoInfoId));
  
    this.personalInfoService.getCategories().subscribe((categories: Category[]) => {
      this.categories = categories;
      this.category = categories[Number(this.informationFinancier.categoryId)-1];
     // console.log(this.category)
   
    });
  
    this.personalInfoService.getActivities().subscribe((activities: Activity[]) => {
      this.activities = activities;
      this.activity  = activities[Number(this.informationFinancier.activityId)-1];
    });

    
    this.personalInfoService.getMonthlyNetIncomes().subscribe((monthlyNetIncomes: MonthlyNetIncome[]) => {
      this.monthlyNetIncomes = monthlyNetIncomes;
      this.monthlyNetIncome  = monthlyNetIncomes[Number(this.informationFinancier.monthlyNetIncomeId)-1];
      console.log(this.informationFinancier.monthlyNetIncomeId)
    });
   //this.getCategory();
    this.setLang();
  }
  
  setModel() {
   
    if (sessionStorage.getItem('pi') !== null) {
      const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
      this.pi = session_pi;
      this.financialInfo = 
      this.pi.financialInfo !== undefined ? this.pi.financialInfo : new FinancialInfo();
      this.financialInfo.personalInfoId = this.pi.id;
    }

  }

  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.incomes = (this.lang === FRENCH_LABEL) ? this.incomes_fr : this.incomes_en;
    this.netIcome = this.incomes[0];

    this.translate.onLangChange.subscribe((data: any) => {
      console.log('onLangChange', data.lang);
      this.lang = data.lang;
      // temporairemnt 
      this.incomes = (this.lang === FRENCH_LABEL) ? this.incomes_fr : this.incomes_en;
      this.netIcome = this.incomes[0];
    });
  }

  findFinancialInfo(id:number){
    this.personalInfoService.getFinancialInfo(id).subscribe(result=>{
      this.informationFinancier=result;
      console.log(this.informationFinancier.categoryId);
    }) 
   }


  continue() {
    // this.financialInfo = new FinancialInfo(1, 302, 3000, this.category.id, this.activity.id);
    this.informationFinancier.activityId = this.activity.id;
    this.informationFinancier.categoryId = this.category.id;
    this.informationFinancier.monthlyNetIncomeId = this.monthlyNetIncome.id;

    //this.financialInfo.personalInfoId =  Number(ls.get("idPersonalInfo"));
    // temp
   // this.financialInfo.monthlyNetIncome = this.netIcome;

    this.personalInfoService.updateFinancialIn(this.informationFinancier).subscribe((result: FinancialInfo) => {
      this.pi.financialInfo = result;
      sessionStorage.setItem('pi', JSON.stringify(this.pi));
    });
    /** to put inside subscribe once service c bon */
    this.router.navigate(['offer/summary']);
  }

  prevStep() {
    this.router.navigate(['offer/summary']);
  }

  getCategory(){
    this.personalInfoService.getCategory(this.informationFinancier.categoryId).subscribe((res:Category)=>{
      this.category = res;
      console.log(this.category)
    })
  }
}

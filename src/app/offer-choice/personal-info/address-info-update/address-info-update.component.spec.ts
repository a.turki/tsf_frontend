import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressInfoUpdateComponent } from './address-info-update.component';

describe('AddressInfoUpdateComponent', () => {
  let component: AddressInfoUpdateComponent;
  let fixture: ComponentFixture<AddressInfoUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddressInfoUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressInfoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { PersonalInfo } from '../../../shared/models/personalInfo';
import { AddressInfo } from '../../../shared/models/addressInfo';
import { CITIES, ls } from 'src/app/shared/settings/AppSettings';
import { FRENCH_LABEL, ENGLISH_LABEL, COUNTRIES_FR_PATH, COUNTRIES_EN_PATH }
  from 'src/app/shared/settings/Constants';

import { Router } from '@angular/router';
import { PersonalInfoService } from '../../../services/personalInfo/personal-info.service';
import { Country } from '../../../services/tfsGlobal/global-state.service';

import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-address-info-update',
  templateUrl: './address-info-update.component.html',
  styleUrls: ['./address-info-update.component.scss']
})
export class AddressInfoUpdateComponent implements OnInit {
  lang?= FRENCH_LABEL;

  pi: PersonalInfo = new PersonalInfo();
  addressInfo: AddressInfo = new AddressInfo();
  address: AddressInfo = new AddressInfo('France', '1 rue de l\'Église, 88000 Vaudéville',
    88000, 'Vaudeville');

  cities: string[] = CITIES;

  countries_fr: Country[] = [];
  countries_en: Country[] = [];
  country?: Country = { "name": "France", "nationality": "Française", "flag": "fr" };
  countries: Country[] = [];
  persoInfoId = ls.get("idPersonalInfo");
  private submitted = false;

  constructor(private personalInfoService: PersonalInfoService, public router: Router,
    public translate: TranslateService, private http: HttpClient) { }

  ngOnInit(): void {
    console.log('country', this.country);
    /*** we need both of them to set our variables */
    forkJoin([
      this.getJSON(COUNTRIES_FR_PATH),
      this.getJSON(COUNTRIES_EN_PATH)]).subscribe(t => {
        this.countries_fr = t[0];
        this.countries = t[0];
        this.countries_en = t[1];
        this.setLang()
      });


    this.getModel(Number(this.persoInfoId));
    /** recuperer le modele depuis la session si il existe */
    this.address.personalInfoId = Number(this.persoInfoId)
    this.setModel();
  }


  getModel(id: number) {
    this.personalInfoService.findAddress(id).subscribe(result => {
      this.addressInfo = result;
      console.log(this.addressInfo.countryOfResidence);
      const c = this.countries.find(({ name }) => name === this.addressInfo.countryOfResidence);
      if (c != null) {
        this.country = c;
      }
    })
  }

  setModel() {
    const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
    this.pi = session_pi;
    this.address = new AddressInfo('France', '1 rue de l\'Église, 88000 Vaudéville',
      88000, 'Vaudeville');
    this.address.personalInfoId = this.pi.id;

  }


  // setLabelsByLang(lang: string) {
  //  this.countries = (lang === ENGLISH_LABEL) ? this.countries_en : this.countries_fr;
  // }

  update() {
    console.log('pi', this.pi);
    this.personalInfoService.updateAddress(this.addressInfo).subscribe((result: AddressInfo) => {
      this.pi.addressInfo = result;
      sessionStorage.setItem('pi', JSON.stringify(this.pi));
      ls.set("pi", this.pi);
    });
  }

  onChangeCountry(country: string) {
    this.country = this.countries.find(item => item.name === country);
    this.personalInfoService.getCities(country);
  }

  continue() {
    if (this.address.address === '' || this.address.zip === undefined || this.address.zip <= 0 || this.address.city === '') {
      this.submitted = true;
      return false;

    }
    this.update();
    this.router.navigate(['offer/summary']);
    return true;

  }

  prevStep() {
    this.router.navigate(['offer/summary']);
  }


  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.setLabelsByLang(this.lang);

    this.translate.onLangChange.subscribe((data: any) => {
      this.lang = data.lang;
      this.setLabelsByLang(data.lang);
    });
  }


  setLabelsByLang(lang: string) {
    if (lang === ENGLISH_LABEL) {
      this.countries = this.countries_en;
      this.country = { "name": "France", "nationality": "French", "flag": "fr" };
    }
    if (lang === FRENCH_LABEL) {
      this.countries = this.countries_fr;
      this.country = { "name": "France", "nationality": "Française", "flag": "fr" };
    }

    this.country = this.countries[0];
    this.address.countryOfResidence = this.country.name;

  }

  getJSON(path: string): Observable<any> {
    return this.http.get(path);
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

}


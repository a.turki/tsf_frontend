import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountOfferUpdateComponent } from './account-offer-update.component';

describe('AccountOfferUpdateComponent', () => {
  let component: AccountOfferUpdateComponent;
  let fixture: ComponentFixture<AccountOfferUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountOfferUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountOfferUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

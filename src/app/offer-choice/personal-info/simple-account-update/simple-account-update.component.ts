import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account/account.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { Account } from 'src/app/shared/models/account';
import { FRENCH_LABEL } from 'src/app/shared/settings/Constants';
import { TranslateService } from '@ngx-translate/core';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { RequestInfo } from 'src/app/shared/models/RequestInfo';
import { ls } from 'src/app/shared/settings/AppSettings';
@Component({
  selector: 'app-simple-account-update',
  templateUrl: './simple-account-update.component.html',
  styleUrls: ['./simple-account-update.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SimpleAccountUpdateComponent implements OnInit, OnDestroy {
  selectedCountryCode = 'eu';
  countryCodes = ['eu','us','gb','ca','dz','bh','dk','le','jp','kw','ly','mr','ma','no','qa','sa','se','ch','ae'];
  accounts!: Account[];
  accounts_checked: Account[]=[];
  lang?= FRENCH_LABEL;
  request: RequestInfo = new RequestInfo ();
  accounts_req: Account[] = [];
  reqId = ls.get("reqId");

  constructor( private personalInfoService: PersonalInfoService,public translate: TranslateService,
    private router: Router,
    public globalStateService: GlobalStateService,
    private accountservice: AccountService
  ) {}

  ngOnInit(): void {
    this.getAccounts(Number(this.reqId));
    this.getAllAccounts();
   this.setLang()
  }

  accountSimpleChoiceToNextStep() {
    this.request.bankAccounts = this.accounts_checked ;
    this.personalInfoService.updateRequest(this.request).subscribe((data) => {
      console.log("done")
      console.log("request"+this.request)
    });
    this.router.navigate(['offer/summary']);
    this.globalStateService.userChoice.personalInfo.activated =
      'active new_style';
    this.globalStateService.userChoice.progressBarValue = 25;
    this.globalStateService.userChoice.accounts = this.accounts_checked;
  }

  accountSimpleChoiceToPrevStep() {
    this.router.navigate(['offer/summary']);
    this.globalStateService.userChoice.personalInfo.activated = '';
    this.globalStateService.userChoice.offer.activated = 'active new_style';
    this.globalStateService.userChoice.progressBarValue = 10;
  }

  getAllAccounts() {
    return this.accountservice.getAllAccounts().subscribe(
      (data) => {
        this.accounts = data;
        if(this.accounts_req[0]!= null || undefined){      this.accounts_checked.push(this.accounts[0]);}
        if(this.accounts_req[1]!= null || undefined){      this.accounts_checked.push(this.accounts[1]);}
        if(this.accounts_req[2]!= null || undefined){      this.accounts_checked.push(this.accounts[2]);}
      },
      (err: any) => console.log('HTTP Error', err),
      () => {
        this.accounts.map((x) => x.id===this.accounts_req[0].id?x.checked=true:x.checked=false||
        x.id===this.accounts_req[1].id?x.checked=true:x.checked=false ||  
        x.id===this.accounts_req[2].id?x.checked=true:x.checked=false);
      }
    );
  }

  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;
    console.log(this.selectedCountryCode)
  }

  ngOnDestroy(): void {
    this.globalStateService.userChoice.offer.activated = 'active';
  }

  setAllAccounts(account_cheched: Account) {
    this.accounts_checked.push(account_cheched)
    this.accounts_checked = this.accounts_checked.filter(x=>x.checked===true);
    console.log(this.accounts_checked);
  }
  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.translate.onLangChange.subscribe((data: any) => {
      console.log('onLangChange', data.lang);
      this.lang = data.lang;
      // temporairemnt 
    });
  }

  getAccounts(id:number){
    this.personalInfoService.getRequest(id).subscribe((data : RequestInfo) => {
      this.request = data;
      this.accounts_req = this.request.bankAccounts !== undefined ? this.request.bankAccounts : [];
      console.log(this.accounts_req[0])
    },(err:any) => console.log('HTTP Error', err),
    () => {    this.getAllAccounts();
    });
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleAccountUpdateComponent } from './simple-account-update.component';

describe('SimpleAccountUpdateComponent', () => {
  let component: SimpleAccountUpdateComponent;
  let fixture: ComponentFixture<SimpleAccountUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimpleAccountUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleAccountUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

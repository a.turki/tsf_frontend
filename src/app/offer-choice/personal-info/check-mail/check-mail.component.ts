import { Component, OnInit } from '@angular/core';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';

@Component({
  selector: 'app-check-mail',
  templateUrl: './check-mail.component.html',
  styleUrls: ['./check-mail.component.scss']
})
export class CheckMailComponent implements OnInit {

  constructor(private personalInfoService: PersonalInfoService, public gsService: GlobalStateService) { }

  ngOnInit(): void {
    this.setHeader()
  }
  sendActivationMail() {
    const session_pi = JSON.parse(sessionStorage.getItem('userChoiceInfo') || '');
    this.personalInfoService.save(session_pi).subscribe(data => {
      console.log("personalInfo", data);
    });

  }
  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 13;
    this.gsService.userChoice._step = 'summary';
  }
}

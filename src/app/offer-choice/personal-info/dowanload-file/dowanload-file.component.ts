import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dowanload-file',
  templateUrl: './dowanload-file.component.html',
  styleUrls: ['./dowanload-file.component.scss']
})
export class DowanloadFileComponent implements OnInit {
@Input() nameFile!: any;
@Input() idFile!: any;
@Input() label!: any;
@Output() deletefilebyId = new EventEmitter<any>();
@Output() dowanloadfilebyId = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }
deleteFile(){
  this.deletefilebyId.emit(this.idFile);

}
downloadFile(){
  this.dowanloadfilebyId.emit(this.idFile);
}


}

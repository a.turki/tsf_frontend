import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DowanloadFileComponent } from './dowanload-file.component';

describe('DowanloadFileComponent', () => {
  let component: DowanloadFileComponent;
  let fixture: ComponentFixture<DowanloadFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DowanloadFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DowanloadFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

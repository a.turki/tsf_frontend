import { Component, OnInit,ViewChild } from '@angular/core';

import {MatAccordion} from '@angular/material/expansion';

@Component({
  selector: 'app-test-accordiant',
  templateUrl: './test-accordiant.component.html',
  styleUrls: ['./test-accordiant.component.scss']
})
export class TestAccordiantComponent implements OnInit {
  @ViewChild(MatAccordion) accordion!: MatAccordion;
  constructor() { }

  ngOnInit(): void {
  }

}

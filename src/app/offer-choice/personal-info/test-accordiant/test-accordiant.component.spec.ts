import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestAccordiantComponent } from './test-accordiant.component';

describe('TestAccordiantComponent', () => {
  let component: TestAccordiantComponent;
  let fixture: ComponentFixture<TestAccordiantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestAccordiantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAccordiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dowanload-file-double-column',
  templateUrl: './dowanload-file-double-column.component.html',
  styleUrls: ['./dowanload-file-double-column.component.scss']
})
export class DowanloadFileDoubleColumnComponent implements OnInit {
  @Input() nameFile!: any;
  @Input() typeFileDoc!: string;
  @Input() idFile!: any;
  @Input() title!: any;
  @Output() deletefilebyId = new EventEmitter<any>();
  @Output() dowanloadfilebyId = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
    
  }
  deleteFile(){
    this.deletefilebyId.emit(this.idFile);
  
  }
  downloadFile(){
    this.dowanloadfilebyId.emit(this.idFile);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DowanloadFileDoubleColumnComponent } from './dowanload-file-double-column.component';

describe('DowanloadFileDoubleColumnComponent', () => {
  let component: DowanloadFileDoubleColumnComponent;
  let fixture: ComponentFixture<DowanloadFileDoubleColumnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DowanloadFileDoubleColumnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DowanloadFileDoubleColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

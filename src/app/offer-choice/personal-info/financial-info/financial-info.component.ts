import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FinancialInfo } from 'src/app/shared/models/financialInfo';
import { PersonalInfoService } from '../../../services/personalInfo/personal-info.service';
import { PersonalInfo } from '../../../shared/models/personalInfo';
import { TranslateService } from '@ngx-translate/core';
import { Category } from 'src/app/shared/models/category';
import { Activity } from 'src/app/shared/models/activity';
import { ls } from 'src/app/shared/settings/AppSettings';
import { FRENCH_LABEL } from 'src/app/shared/settings/Constants';
import { MonthlyNetIncome } from 'src/app/shared/models/MonthlyNetIncome';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';

@Component({
  selector: 'app-financial-info',
  templateUrl: './financial-info.component.html',
  styleUrls: ['./financial-info.component.scss']
})
export class FinancialInfoComponent implements OnInit {
  lang?= FRENCH_LABEL;
  pi: PersonalInfo = new PersonalInfo();
  financialInfo: FinancialInfo = new FinancialInfo();
  monthlyNetIncomes: MonthlyNetIncome[] = [];
  category: Category = new Category();
  activity: Activity = new Activity();
  categories: Category[] = [];
  activities: Activity[] = [];
  monthlyNetIncome: MonthlyNetIncome = new MonthlyNetIncome();

  incomes_fr: string[] = ['Entre 1500 et 3000', 'Entre 3000 et 4500', 
  'Entre 4500 et 6000', 'Entre 6000 et 7500'];
  incomes_en: string[] = ['Between 1500 and 3000', 'Between 3000 and 4500', 
  'Between 4500 and 6000', 'Between 6000 and 7500'];

  incomes : string[] = [];
  netIcome: string = '';

  /**
   * 
   * @param router personalInfoId: number, monthlyNetIncome: number, categoryId: number,
        activityId: number
   * @param personalInfoService 
   */

  constructor(public router: Router, private personalInfoService: PersonalInfoService,
    public translate: TranslateService,  public gsService: GlobalStateService) { }

  ngOnInit(): void {
    /** recuperer le modele depuis la session si il existe */
    this.setModel();

    this.personalInfoService.getCategories().subscribe((categories: Category[]) => {
      this.categories = categories;
      this.category = categories[0];
    });

    this.personalInfoService.getActivities().subscribe((activities: Activity[]) => {
      this.activities = activities;
      this.activity  = activities[0];
    });

    this.personalInfoService.getMonthlyNetIncomes().subscribe((monthlyNetIncomes: MonthlyNetIncome[]) => {
      this.monthlyNetIncomes = monthlyNetIncomes;
      console.log('monthlyNetIncomes'+this.monthlyNetIncomes)
      this.monthlyNetIncome  = monthlyNetIncomes[0];
    });

    this.setLang();
    this.setHeader()
  }

  setModel() {
   
    if (sessionStorage.getItem('pi') !== null) {
      const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
      this.pi = session_pi;
      this.financialInfo = 
      this.pi.financialInfo !== undefined ? this.pi.financialInfo : new FinancialInfo();
      this.financialInfo.personalInfoId = this.pi.id;
      console.log(this.financialInfo.personalInfoId)
    }

  }

  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.incomes = (this.lang === FRENCH_LABEL) ? this.incomes_fr : this.incomes_en;
    this.netIcome = this.incomes[0];

    this.translate.onLangChange.subscribe((data: any) => {
      console.log('onLangChange', data.lang);
      this.lang = data.lang;
      // temporairemnt 
      this.incomes = (this.lang === FRENCH_LABEL) ? this.incomes_fr : this.incomes_en;
      this.netIcome = this.incomes[0];
    });
  }

  continue() {
    // this.financialInfo = new FinancialInfo(1, 302, 3000, this.category.id, this.activity.id);
    this.financialInfo.activityId = this.activity.id;
    this.financialInfo.categoryId = this.category.id;
    this.financialInfo.monthlyNetIncomeId = this.monthlyNetIncome.id;
    //this.financialInfo.personalInfoId =  Number(ls.get("idPersonalInfo"));
    // temp
    //this.financialInfo.monthlyNetIncome = this.netIcome;

    this.personalInfoService.updateFinancialInfo(this.financialInfo).subscribe((result: FinancialInfo) => {
      this.pi.financialInfo = result;
      sessionStorage.setItem('pi', JSON.stringify(this.pi));
    });
    /** to put inside subscribe once service c bon */
    this.router.navigate(['offer/agency-info']);
  }

  prevStep() {
    this.router.navigate(['offer/address-info']);
  }

  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 38;
    this.gsService.userChoice._step = 'financial-info';

    
  }
}

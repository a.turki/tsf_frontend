import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencyInfoUpdateComponent } from './agency-info-update.component';

describe('AgencyInfoUpdateComponent', () => {
  let component: AgencyInfoUpdateComponent;
  let fixture: ComponentFixture<AgencyInfoUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgencyInfoUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgencyInfoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

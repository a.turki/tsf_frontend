import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { Agency } from 'src/app/shared/models/agency';
import { Governement } from 'src/app/shared/models/Government';
import { Municipality } from 'src/app/shared/models/Municipality';
import { PersonalInfo } from 'src/app/shared/models/personalInfo';
import { ls } from 'src/app/shared/settings/AppSettings';

@Component({
  selector: 'app-agency-info-update',
  templateUrl: './agency-info-update.component.html',
  styleUrls: ['./agency-info-update.component.scss']
})
export class AgencyInfoUpdateComponent implements OnInit {
  pi: PersonalInfo = new PersonalInfo();
  /**agency: Agency = new Agency('Amilcar','Ariana - Agence Menzah 6 (147, Avenue Othmen Ibn Affen - El Menzah 6, 2091)',
  1003, 'Tunis');*/
  personalInfo: PersonalInfo = new PersonalInfo();
  agency: Agency = new Agency();
  persoInfoId =ls.get("idPersonalInfo") ;
  government: Governement = new Governement();
  municipality: Municipality = new Municipality();
  agencyChoice: Agency = new Agency (); 
  governments: Governement[] = [];
  municipalities: Municipality[] = [];
  governmentUP: Governement = new Governement();
  agencies: Agency[] = [];
  isAgencySelectFocused: any;
  municipalityUP: Municipality = new Municipality();

  constructor( private personalInfoService: PersonalInfoService,
    public router: Router) { }

  ngOnInit(): void {
       /** recuperer le modele depuis la session si il existe */
       this.setModel();
       this.personalInfoService.getGovernements().subscribe((res) => {
         console.log('res', res);
         // gestion de cas res null
         this.governments = res;
         this.getPersonalInfo(Number(this.persoInfoId));
       });
     
  }

  setModel() {

    if (sessionStorage.getItem('pi') !== null) {
      const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
      this.pi = session_pi;
      this.agency =
      (this.pi.agency !== undefined) ? this.pi.agency : new Agency();
      this.agency.personalInfoId = this.pi.id;
    }
    // this.personalInfoService.findByPersonalId(Number(ls.get("idPersonalInfo"))).subscribe(data=>{console.log("data",data)
    // this.pi = data;
    //   });

  }

  continue() {
    this.personalInfo.agencyId = this.agency.id;
    this.personalInfoService.update(this.personalInfo).subscribe((pi: PersonalInfo) => {
      console.log("done")
      this.pi = pi;
      this.pi.agency = this.agency;
      sessionStorage.setItem('pi', JSON.stringify(this.pi));
    });
    this.router.navigate(['offer/summary']);
  }

  prevStep() {
    this.router.navigate(['offer/summary']);
  }

  OnChangeGov(value: any) {
    this.municipalities =
    this.government.municipalities !== undefined ? this.government.municipalities : [];
    this.municipality = this.municipalities[0];
    this.agencies = this.municipality.agencies !== undefined ? this.municipality.agencies : [];
    this.agency = this.agencies[0];
  }

  OnChangeMun(value: any) {
    this.agencies = this.municipality.agencies !== undefined ? this.municipality.agencies : [];
    this.agency = this.agencies[0];
  }

  focusAgencySelect() {
    this.isAgencySelectFocused = true;
  }
  getPersonalInfo(id:number){
    this.personalInfoService.findByPersonalId(id).subscribe(result=>{
    this.personalInfo=result;
    console.log(this.personalInfo.agencyId)
  }, (error) => {
    console.log(error);
  },()=>{
    console.log('Fini 1!');
    this.personalInfoService.getAgency(this.personalInfo.agencyId).subscribe((result:Agency)=>{
      console.log(result.address)     
      this.agencyChoice = result;
    }, (error) => {
      console.log(error);
    },()=>{
      console.log('Fini 2!');
      this.personalInfoService.getMunicipalite(this.agencyChoice.municipalityId).subscribe((result:Municipality)=>{
        this.municipalityUP = result;
        console.log(this.municipalityUP.name)
    }, (error) => {
      console.log(error);
    },()=>{
      console.log('Fini 3!');
      this.personalInfoService.getGoverment(this.municipalityUP.governorateId).subscribe((result:Governement)=>{
        this.governmentUP = result;
        console.log(this.governmentUP.name)
      }, (error) => {
        console.log(error);
      },()=>{       
         const d = this.governments.find(({id}) => id === this.governmentUP.id);
      if (d != null) {
        this.government = d;
        console.log("d"+d)
      }
         this.municipalities =
                this.government.municipalities !== undefined ? this.government.municipalities : [];
                const c = this.municipalities.find(({id}) => id === this.agencyChoice.municipalityId);
                if (c != null) {
                  this.municipality = c;
                  console.log("c"+c)

                }
         this.agencies = this.municipality.agencies !== undefined ? this.municipality.agencies : [];
         const u = this.agencies.find(({id}) => id === this.agencyChoice.id);
         if (u != null) {
          console.log("u"+u)

           this.agency = u;}
  
       })
    })})
  });
 
  }

  getAgency(){
    this.personalInfoService.getAgency(this.personalInfo.agencyId).subscribe((result:Agency)=>{
      console.log(result.municipalityId)     
      this.agencyChoice = result;
    })
  }

  getGoverment(id:any){
    this.personalInfoService.getGoverment(id).subscribe((result:Governement)=>{
      this.governmentUP = result;
    })
  }
  getMunicipalite(id:any){
    this.personalInfoService.getMunicipalite(id).subscribe((result:Governement)=>{
      this.municipalityUP = result;
    })   
  }

}

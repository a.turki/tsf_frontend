import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationMailComponent } from './validation-mail.component';

describe('ValidationMailComponent', () => {
  let component: ValidationMailComponent;
  let fixture: ComponentFixture<ValidationMailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationMailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

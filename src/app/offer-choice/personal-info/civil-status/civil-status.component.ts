import { Component, OnInit, AfterViewChecked, Inject } from '@angular/core';
import { PersonalInfo } from '../../../shared/models/personalInfo';
import { PersonalInfoService } from '../../../services/personalInfo/personal-info.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { FatcaDialogComponent } from './fatca-dialog/fatca-dialog.component';
import { FRENCH_LABEL } from 'src/app/shared/settings/Constants';
import { ENGLISH_LABEL } from 'src/app/shared/settings/Constants';
import { ls } from 'src/app/shared/settings/AppSettings';


export interface Nationality {
  name: string;
  nationality: string;
  flag: string;

}

@Component({
  selector: 'app-civil-status',
  templateUrl: './civil-status.component.html',
  styleUrls: ['./civil-status.component.scss']
})
export class CivilStatusComponent implements OnInit, AfterViewChecked {
  FLAGS_EN_PATH = '../../../../assets/flags/FlagsEn.json';
  FLAGS_FR_PATH = '../../../../assets/flags/flagsFr.json';
  lang?= FRENCH_LABEL;
  secondNAtio = true;
  nationality?= { name: 'France', nationality: 'Française', flag: 'fr' };
  nationalities_fr: Nationality[] = [];
  nationalities_en: Nationality[] = [];
  nationalities: Nationality[] = [];
  idRequest: any = this.route.snapshot.paramMap.get('id');
  reqId:any;
  civilities_fr = ['Célibataire', 'Marié(e)', 'Divorcé(e)', 'Veuf(ve)'];
  civilities_en = ['Single', 'Maried', 'Divorced', 'Widower'];
  civilities = this.civilities_fr;

  children = [{
    label: '0',
    nb: 0,
    active: false
  },
  {
    label: '1',
    nb: 1,
    active: false
  },
  {
    label: '2',
    nb: 2,
    active: false
  },
  {
    label: '3',
    nb: 3,
    active: false
  },
  {
    label: '+4',
    nb: 4,
    active: false
  },

  ];

  pi: PersonalInfo = new PersonalInfo();

  showSecondNationality = false;

  constructor(private personalInfoService: PersonalInfoService, public router: Router,
    private route: ActivatedRoute,
    public gsService: GlobalStateService, public translate: TranslateService,
    private http: HttpClient, public dialog: MatDialog) {

    /* we need to verify if requestId exist or not */
    try { window.atob(this.idRequest) 
      this.reqId = atob(this.idRequest) ;
    }
    catch (e) {
      this.router.navigate(['']);
    }

    
  }

  ngOnInit(): void {
    this.setModel();
    console.log(this.reqId);
    ls.set("idRequest", this.idRequest)
    /** recuperer le modele depuis la session si il existe */
    // this.personalInfoService.findByRequestId(Number(this.reqId)).subscribe(data=>{console.log("data",data)
    // this.pi = data;
    //   });

    /*** we need both of them to set our variables */
    forkJoin([
      this.getJSON(this.FLAGS_FR_PATH),
      this.getJSON(this.FLAGS_EN_PATH)]).subscribe(t => {
        this.nationalities_fr = t[0];
        this.nationalities = t[0];
        this.nationalities_en = t[1];
        this.setLang();
        console.log('pi', this.pi);
        console.log('t', t);
        console.log('nationalities_fr', this.nationalities_fr);
        console.log('nationalities_en', this.nationalities_en);
        console.log('nationalities', this.nationalities);
      });

  }

  ngAfterViewChecked(): void {
    this.setHeader();

  }

  setModel() {
    if (sessionStorage.getItem('pi') !== null) {
      const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
      this.pi = session_pi;
      console.log('pi' + this.pi);
      console.warn('77888888')
      console.warn('77888888')
    } else {
      this.personalInfoService.findByRequestId(Number(this.reqId)).subscribe(data => {
        console.log('data', data);
        this.pi = data;
      });
    }

  }
  setHeader() {
    this.gsService.userChoice.personalInfo.activated = 'active new_style';
    this.gsService.userChoice.offer.activated = 'active';
    this.gsService.userChoice.progressBarValue = 30;
    this.gsService.userChoice._step = 'civil-status';
  }

  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.setLabelsByLang(this.lang);

    this.translate.onLangChange.subscribe((data: any) => {
      console.log('onLangChange', data.lang);
      this.lang = data.lang;
      this.setLabelsByLang(data.lang);
    });
  }

  setLabelsByLang(lang: string) {
    if (lang === ENGLISH_LABEL) {
      this.pi.nationality = 'Tunisian';
      this.pi.secondNationality = 'FRENCH';
      this.nationality = { name: 'France', nationality: 'Française', flag: 'fr' };
      this.pi.maritalStatus = 'Single';
      this.civilities = this.civilities_en;
      this.nationalities = this.nationalities_en;
    }
    if (lang === FRENCH_LABEL) {
      this.pi.nationality = 'Tunisienne';
      this.pi.secondNationality = 'FRANCE';
      this.nationality = { name: 'France', nationality: 'Française', flag: 'fr' };
      this.pi.maritalStatus = 'Célibataire';
      this.civilities = this.civilities_fr;
      this.nationalities = this.nationalities_fr;
    }

    this.pi.nbrkids = 0;
    this.pi.americanIndex = false;
  }

  setActive(index: number) {
    this.children.map(x => x.active = false);
    this.children[index].active = true;
    this.pi.nbrkids = this.children[index].nb;

  }

  checkValue(event: any) {
    console.log(event);
  }

  onChangeNationality(nationality: any) {
    this.nationality = (this.lang === FRENCH_LABEL) ?
      this.nationalities_fr.find(item => item.name === nationality) :
      this.nationalities_en.find(item => item.name === nationality);


  }

  openDialog() {
    const dialogRef = this.dialog.open(FatcaDialogComponent);

    dialogRef.afterClosed().subscribe((result: boolean) => {
      this.pi.americanIndex = result;
    });
  }

  continue() {
    this.personalInfoService.update(this.pi).subscribe(data => {
      sessionStorage.setItem('pi', JSON.stringify(data));
      ls.set('idPersonalInfo', (this.pi.id));
      ls.set('reqId', this.reqId);
      ls.set('idRequest', this.idRequest);
    });

    /** to put inside subscribe once service c bon */
    this.router.navigate(['offer/address-info']);
  }

  prevStep() {
    this.router.navigate(['offer/step3']);
  }



  getJSON(path: string): Observable<any> {
    return this.http.get(path);
  }
  secondNatio(event: any) {
    console.log(event.target.checked);
    if (event.target.checked) {
      this.secondNAtio = false;
    }

  }

  closeSecondNationality() {
    this.showSecondNationality = false
    this.secondNAtio = true
  }

}




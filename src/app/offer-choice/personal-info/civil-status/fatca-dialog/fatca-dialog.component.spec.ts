import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FatcaDialogComponent } from './fatca-dialog.component';

describe('FatcaDialogComponent', () => {
  let component: FatcaDialogComponent;
  let fixture: ComponentFixture<FatcaDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FatcaDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FatcaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

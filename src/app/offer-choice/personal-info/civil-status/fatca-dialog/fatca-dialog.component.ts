import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-fatca-dialog',
  templateUrl: './fatca-dialog.component.html',
  styleUrls: ['./fatca-dialog.component.scss']
})
export class FatcaDialogComponent implements OnInit {
hintsList: string[] = [];
fatcaDocsList: string[] = [];

  constructor(public translate: TranslateService) { }

  ngOnInit(): void {
    const fatcaHints = this.translate.instant('fatcaHints');
    const fatcaDocsList = this.translate.instant('fatcaDocsList');
    this.hintsList = fatcaHints.split('|');
    this.fatcaDocsList = fatcaDocsList.split('|');
  }

}

import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {AfterContentInit, AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormGroup, FormBuilder, Validators, NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { PersonalInfo } from 'src/app/shared/models/personalInfo';
import { ls } from 'src/app/shared/settings/AppSettings';
import { COUNTRIES_CODE } from 'src/app/shared/settings/Constants';
import { UserInfoChoice } from 'src/app/shared/types/UserInfoChoice';
import {IntlTelInputComponent} from 'intl-tel-input-ng';

export interface NativeCountry {
  name: string;
  flag: string;
}
@Component({
  selector: 'app-civility-step-update',
  templateUrl: './civility-step-update.component.html',
  styleUrls: ['./civility-step-update.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CivilityStepUpdateComponent  implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChild(IntlTelInputComponent)
  phoneNumberComp!: IntlTelInputComponent;
  countryCodes = COUNTRIES_CODE;
  setted = false;
  personalInfoForm!: FormGroup;
  selectedCountryCode = 'us';
  NATIVE_COUNTRY_FLAG_FR_PATH = '../../../../assets/flags/nativeCountriesFR-test.json';
  NATIVE_COUNTRY_FLAG_EN_PATH = '../../../../assets/flags/nativeCountriesEN-test.json';
  selectedNativeCountry :NativeCountry = {name:"Françe",flag:"fr"};
  nativeCountries_fr: NativeCountry[] = [];
  nativeCountries_en: NativeCountry[] = [];
  nativeCountries: NativeCountry[] = [];
  submitted = false;
  E164PhoneNumber = '';
  userChoiceInfo = new UserInfoChoice  ;
  isMailInputFocused = false;
  isPhoneInputFocused = false;
  persoInfoId =ls.get("idPersonalInfo") ;
  personalInfo : PersonalInfo = new PersonalInfo();
  constructor(private http: HttpClient,
    public globalStateService: GlobalStateService,
    private formBuilder: FormBuilder,
    private personalInfoService:PersonalInfoService,
    private router:Router
  ) {}

  ngOnInit(): void {
    this.getModel(Number(this.persoInfoId));

    this.personalInfoForm = this.formBuilder.group({
      civility: ['', Validators.required],
      firstName: ['', [Validators.required,
        Validators.maxLength(50),
        Validators.pattern('^[a-zA-Z ]*$')]],
      lastName: ['', [Validators.required,
        Validators.maxLength(50),
        Validators.pattern('^[a-zA-Z ]*$')]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      nativeCountry: ['', Validators.required],
      birthday: ['']
      // clientABT: [''],
      // rib: [''],
      // nationality: [''],
      // secondNationality: [''],
      // maritalStatus: [''],
      // nbrKids: [''],
      // americanIndex: [''],
    });


     /*** we need both of them to set our variables */
     forkJoin([
      this.getJSON(this.NATIVE_COUNTRY_FLAG_FR_PATH),
      this.getJSON(this.NATIVE_COUNTRY_FLAG_EN_PATH)]).subscribe(t=> {
        this.nativeCountries_fr = t[0];
        this.nativeCountries = t[0];
        this.nativeCountries_en = t[1];
        this.setValues();
    });

  }

  getModel(id:number){
    this.personalInfoService.findByPersonalId(id).subscribe(result =>{
      this.personalInfo = result;
      console.log(this.personalInfo.phone); console.log(this.personalInfo.phone);
    })
  }

  setValues() {
    //this.personalInfoForm.set("phone")?.setValue(this.personalInfo.phone);
    this.personalInfoForm.patchValue({
      civility:this.personalInfo.civility,
      firstName:this.personalInfo.firstName,
      lastName: this.personalInfo.lastName,
      email: this.personalInfo.email,
      phone: this.personalInfo.phone,
      nativeCountry: this.personalInfo.nativeCountry,
      birthday: this.personalInfo.birthday
    });
  }


  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;
  }

  focusMailInput(){
    this.isMailInputFocused =true;
  }
  focusPhoneInput(){
    this.isPhoneInputFocused =true;
    console.log(this.isPhoneInputFocused);

  }
  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  ngOnDestroy(): void {
    //  this.globalStateService.userChoice.personalInfo.activated=false;
  }


  personalInfoToNextStep() {
    this.submitted = true;
    this.personalInfoForm.get("phone")?.setValue(this.E164PhoneNumber)


    this.personalInfo.civility =  this.personalInfoForm.get("civility")?.value;
    this.personalInfo.firstName = this.personalInfoForm.get("firstName")?.value;
    this.personalInfo.lastName = this.personalInfoForm.get("lastName")?.value;
    this.personalInfo.email = this.personalInfoForm.get("email")?.value;
    this.personalInfo.phone = this.personalInfoForm.get("phone")?.value;
    this.personalInfo.birthday= this.personalInfoForm.get("birthday")?.value;
    this.userChoiceInfo.langue = "fr";
    this.personalInfo.nativeCountry = this.personalInfoForm.get("nativeCountry")?.value;
    this.userChoiceInfo.accounts = this.globalStateService.userChoice.accounts;

    if(this.personalInfoForm.invalid){
      return;
    }
    console.log("firsname"+this.userChoiceInfo.firstName)
    this.personalInfoService.update(this.personalInfo).subscribe((data: PersonalInfo) => {
      console.log("personalInfo",data);
      sessionStorage.setItem('pi', JSON.stringify(data));
    });
    this.router.navigate(['offer/summary']);
    this.globalStateService.userChoice.personalInfo.activated="active new_style";
    this.globalStateService.userChoice.offer.activated="active";
    this.globalStateService.userChoice.progressBarValue=25;
  }

  personalInfoToPrevStep(){
    this.router.navigate(['offer/summary']);
    this.globalStateService.userChoice.personalInfo.activated="";
    this.globalStateService.userChoice.offer.activated="active new_style";
    this.globalStateService.userChoice.progressBarValue=10;
  }

  public getJSON(path: string): Observable<any> {
    return this.http.get(path);
  }


  getSelectedFlag(){

    let obj = this.nativeCountries.find(x=>x.name===this.personalInfoForm.get('nativeCountry')!.value)

     return {name:obj?.name,flag:obj?.flag}
  }


  get f() { return this.personalInfoForm.controls; }

  ngAfterViewChecked(){
    if (!this.setted){
      if (this.personalInfo.phone != null) {
        this.phoneNumberComp.phoneNumber = this.personalInfo.phone;
        this.setted = true;
      }
    }
  }
  validation(form: NgForm) {
    if (form.invalid)  {
      document.getElementsByName('phone')[0].style.border = 'solid 1px #f44336';
    }
    else {
      document.getElementsByName('phone')[0].style.border = 'solid 1px #d6d6d6';
    }
  }
}

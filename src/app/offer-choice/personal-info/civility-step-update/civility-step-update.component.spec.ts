import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CivilityStepUpdateComponent } from './civility-step-update.component';

describe('CivilityStepUpdateComponent', () => {
  let component: CivilityStepUpdateComponent;
  let fixture: ComponentFixture<CivilityStepUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CivilityStepUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CivilityStepUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

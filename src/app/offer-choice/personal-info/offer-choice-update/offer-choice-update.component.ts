import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OfferService } from 'src/app/services/offer/offer.service';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { Offer } from 'src/app/shared/models/offer';
import { RequestInfo } from 'src/app/shared/models/RequestInfo';
import { ls } from 'src/app/shared/settings/AppSettings';

@Component({
  selector: 'app-offer-choice-update',
  templateUrl: './offer-choice-update.component.html',
  styleUrls: ['./offer-choice-update.component.scss']
})
export class OfferChoiceUpdateComponent implements OnInit,OnDestroy {
  offers: Offer[] = [];
  request: RequestInfo = new RequestInfo (); 
  reqId = ls.get("reqId");
  constructor(private router: Router ,private offerService: OfferService,public globalStateService:GlobalStateService, private personalInfoService: PersonalInfoService) {
    this.globalStateService.userChoice.offer.activated = 'active new_style';
   }


  ngOnInit(): void {
    console.log(this.globalStateService.userChoice.progressBarValue)
    this.getOffers();
    this.getRequest(Number(this.reqId));


  }

  getOffers() {
    this.offerService.getOffers().subscribe((data) => {
      this.offers = data;
    });
  }

  getRequest(id:number){
    this.personalInfoService.getRequest(id).subscribe((data) => {
      this.request = data;
    });
  }






  packChoiceToNextStep(item:Offer){
    this.globalStateService.userChoice._step = 'target_account';
    /*this.globalStateService.userChoice.offer.id = item.id;
   this.globalStateService.userChoice.offer.detailOffers=item.detailOffers;
   this.globalStateService.userChoice.offer.name=item.name;
   this.globalStateService.userChoice.offer.price=item.price;
   this.globalStateService.userChoice.offer.url=item.url;
*/
    this.request.offerId = item.id;
    this.personalInfoService.updateRequest(this.request).subscribe((data) => {
      console.log("done")
      console.log("request"+this.request)
    });
   this.router.navigate(['offer/AccountOfferUpdate']);
  }

  ngOnDestroy(): void {

  }

}
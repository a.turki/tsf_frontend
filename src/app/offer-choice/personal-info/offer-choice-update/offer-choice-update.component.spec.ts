import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferChoiceUpdateComponent } from './offer-choice-update.component';

describe('OfferChoiceUpdateComponent', () => {
  let component: OfferChoiceUpdateComponent;
  let fixture: ComponentFixture<OfferChoiceUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferChoiceUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferChoiceUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

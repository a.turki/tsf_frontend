import { Component, OnInit } from '@angular/core';
import { PersonalInfo } from '../../../shared/models/personalInfo';
import { PersonalInfoService } from '../../../services/personalInfo/personal-info.service';
import { Agency } from 'src/app/shared/models/agency';

import { Router } from '@angular/router';
import { ls } from 'src/app/shared/settings/AppSettings';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { Governement } from 'src/app/shared/models/Government';
import { Municipality } from 'src/app/shared/models/Municipality';

@Component({
  selector: 'app-agency',
  templateUrl: './agency.component.html',
  styleUrls: ['./agency.component.scss']
})
export class AgencyComponent implements OnInit {
  pi: PersonalInfo = new PersonalInfo();
  /**agency: Agency = new Agency('Amilcar','Ariana - Agence Menzah 6 (147, Avenue Othmen Ibn Affen - El Menzah 6, 2091)',
  1003, 'Tunis');*/

  agency: Agency = new Agency();

  government: Governement = new Governement();
  municipality: Municipality = new Municipality();

  governments: Governement[] = [];
  municipalities: Municipality[] = [];

  agencies: Agency[] = [];
  isAgencySelectFocused: any;

  constructor( private personalInfoService: PersonalInfoService,
    public router: Router,   public gsService: GlobalStateService) { }

  ngOnInit(): void {
       /** recuperer le modele depuis la session si il existe */
       this.setHeader()
       this.setModel();
       this.personalInfoService.getGovernements().subscribe((res) => {
         console.log('res', res);
         // gestion de cas res null
         this.governments = res;
         this.government = this.governments[0];
         this.municipalities =
         this.government.municipalities !== undefined ? this.government.municipalities : [];
         this.municipality = this.municipalities[0];
         this.agencies = this.municipality.agencies !== undefined ? this.municipality.agencies : [];
         this.agency = this.agencies[0];

       });
      
  }

  setModel() {

    if (sessionStorage.getItem('pi') !== null) {
      const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
      this.pi = session_pi;
      this.agency =
      (this.pi.agency !== undefined) ? this.pi.agency : new Agency();
      this.agency.personalInfoId = this.pi.id;
    }
  }

  continue() {
    this.pi.agencyId = this.agency.id;
    this.personalInfoService.update(this.pi).subscribe((pi: PersonalInfo) => {
      console.log("done")
      this.pi = pi;
      this.pi.agency = this.agency;
      sessionStorage.setItem('pi', JSON.stringify(this.pi));
    });
    this.router.navigate(['offer/summary']);
  }

  prevStep() {
    this.router.navigate(['offer/financial-info']);
  }

  OnChangeGov(value: any) {
    this.municipalities =
    this.government.municipalities !== undefined ? this.government.municipalities : [];
    this.municipality = this.municipalities[0];
    this.agencies = this.municipality.agencies !== undefined ? this.municipality.agencies : [];
    this.agency = this.agencies[0];
  }

  OnChangeMun(value: any) {
    this.agencies = this.municipality.agencies !== undefined ? this.municipality.agencies : [];
    this.agency = this.agencies[0];
  }

  focusAgencySelect() {
    this.isAgencySelectFocused = true;
  }


  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 42;
    this.gsService.userChoice._step = 'agency-info';
  }
}

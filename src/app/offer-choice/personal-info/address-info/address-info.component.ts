import { Component, OnInit } from '@angular/core';
import { PersonalInfo } from '../../../shared/models/personalInfo';
import { AddressInfo } from '../../../shared/models/addressInfo';
import { CITIES, ls } from 'src/app/shared/settings/AppSettings';
import { FRENCH_LABEL, ENGLISH_LABEL, COUNTRIES_FR_PATH, COUNTRIES_EN_PATH }
  from 'src/app/shared/settings/Constants';

import { Router } from '@angular/router';
import { PersonalInfoService } from '../../../services/personalInfo/personal-info.service';
import { Country, GlobalStateService } from '../../../services/tfsGlobal/global-state.service';

import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-address-info',
  templateUrl: './address-info.component.html',
  styleUrls: ['./address-info.component.scss']
})
export class AddressInfoComponent implements OnInit {
  lang?= FRENCH_LABEL;
  reqId = ls.get('reqId');
  idRequest = ls.get('idRequest');
  pi: PersonalInfo = new PersonalInfo();
  address: AddressInfo = new AddressInfo();

  cities: string[] = CITIES;

  countries_fr: Country[] = [];
  countries_en: Country[] = [];
  country?: Country = { "name": "France", "nationality": "Française", "flag": "fr" };
  countries: Country[] = [];
  private submitted = false;


  constructor(private personalInfoService: PersonalInfoService, public router: Router,
    public translate: TranslateService, private http: HttpClient, public gsService: GlobalStateService) { }

  ngOnInit(): void {
    console.log('country', this.country);
    /*** we need both of them to set our variables */
    forkJoin([
      this.getJSON(COUNTRIES_FR_PATH),
      this.getJSON(COUNTRIES_EN_PATH)]).subscribe(t => {
        this.countries_fr = t[0];
        this.countries = t[0];
        this.countries_en = t[1];
        this.setLang()
      });

      this.setHeader()

    /** recuperer le modele depuis la session si il existe */
   this.address.personalInfoId = Number(ls.get("idPersonalInfo"))
   this.setModel();
  }


  setModel() {
    if (sessionStorage.getItem('pi') !== null) {
      const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
      this.pi = session_pi;
      this.address =
      (this.pi.addressInfo !== undefined) ? this.pi.addressInfo : new AddressInfo();
      this.address.personalInfoId = this.pi.id;
    }
  }


  // setLabelsByLang(lang: string) {
  //  this.countries = (lang === ENGLISH_LABEL) ? this.countries_en : this.countries_fr;
  // }

  update() {
    console.log('pi', this.pi);
    this.personalInfoService.updateAdd(this.address).subscribe((result: AddressInfo) => {
      this.pi.addressInfo = result;
      sessionStorage.setItem('pi', JSON.stringify(this.pi));
      ls.set("pi",this.pi);
    });
  }

  onChangeCountry(country: string) {
    this.country = this.countries.find(item => item.name === country);
    this.personalInfoService.getCities(country);
  }

  continue() {
    if (this.address.address === '' || this.address.zip === undefined || this.address.zip <= 0 || this.address.city === '')
    {
      this.submitted = true;
      return false;

    }
    this.update();
    this.router.navigate(['offer/financial-info']);
    return true;
  }

  prevStep() {
    this.router.navigate(['offer/civil-status/'+this.idRequest]);
  }


  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.setLabelsByLang(this.lang);

    this.translate.onLangChange.subscribe((data: any) => {
      this.lang = data.lang;
      this.setLabelsByLang(data.lang);
    });
  }


  setLabelsByLang(lang: string) {
    if (lang === ENGLISH_LABEL) {
      this.countries = this.countries_en;
      this.country =  {  "name": "France", "nationality": "French", "flag": "fr" };
    }
    if (lang === FRENCH_LABEL) {
      this.countries = this.countries_fr;
      this.country =  {  "name": "France", "nationality": "Française", "flag": "fr" };
    }

    this.country = this.countries[0];
    this.address.countryOfResidence = this.country.name;

  }

  getJSON(path: string): Observable<any> {
    return this.http.get(path);
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 34;
    this.gsService.userChoice._step = 'address-info';

  }
}

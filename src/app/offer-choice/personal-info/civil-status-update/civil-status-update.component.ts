import { HttpClient } from '@angular/common/http';
import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable } from 'rxjs';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { PersonalInfo } from 'src/app/shared/models/personalInfo';
import { ls } from 'src/app/shared/settings/AppSettings';
import { FRENCH_LABEL, ENGLISH_LABEL } from 'src/app/shared/settings/Constants';
import { FatcaDialogComponent } from '../civil-status/fatca-dialog/fatca-dialog.component';
export interface Nationality {
  name: string;
  nationality: string;
  flag: string;
  
}
@Component({
  selector: 'app-civil-status-update',
  templateUrl: './civil-status-update.component.html',
  styleUrls: ['./civil-status-update.component.scss']
})
export class CivilStatusUpdateComponent  implements OnInit, AfterViewChecked {
  checked:boolean = false;
  FLAGS_EN_PATH = '../../../../assets/flags/flagsEN-test-test.json';
  FLAGS_FR_PATH = '../../../../assets/flags/flagsFR-test-test.json';
  lang?= FRENCH_LABEL;
  secondNAtio = true;
  nationality?= { name: 'France', nationality: 'Française', flag: 'fr' };
  nationalities_fr: Nationality[] = [];
  nationalities_en: Nationality[] = [];
  nationalities: Nationality[] = [];
  reqId = this.route.snapshot.paramMap.get('id');
  persoInfoId =ls.get("idPersonalInfo") ;
  civilities_fr = ['Célibataire', 'Marié(e)', 'Divorcé(e)', 'Veuf(ve)'];
  civilities_en = ['Single', 'Maried', 'Divorced', 'Widower'];
  civilities = this.civilities_fr;
  personalInfo: PersonalInfo = new PersonalInfo();
  children = [{
    label: "0",
    nb: 0,
    active: false
  },
  {
    label: "1",
    nb: 1,
    active: false
  },
  {
    label: "2",
    nb: 2,
    active: false
  },
  {
    label: "3",
    nb: 3,
    active: false
  },
  {
    label: "4+",
    nb: 4,
    active: false
  },

  ];

  pi: PersonalInfo = new PersonalInfo();

  showSecondNationality = false;

  constructor(private personalInfoService: PersonalInfoService, public router: Router,
    private route: ActivatedRoute,
    public gsService: GlobalStateService, public translate: TranslateService,
    private http: HttpClient, public dialog: MatDialog) {

    /* we need to verify if requestId exist or not */
    // this.personalInfoService.getRequest(Number(this.reqId)).subscribe(data=>{console.log("data",data)});
  }

  ngOnInit(): void {
    this.getPersonalInfo(Number(this.persoInfoId));
    this.setModel();
    
    /** recuperer le modele depuis la session si il existe */
    // this.personalInfoService.findByRequestId(Number(this.reqId)).subscribe(data=>{console.log("data",data)
    // this.pi = data;
    //   });

    /*** we need both of them to set our variables */
    forkJoin([
      this.getJSON(this.FLAGS_FR_PATH),
      this.getJSON(this.FLAGS_EN_PATH)]).subscribe(t => {
        this.nationalities_fr = t[0];
        this.nationalities = t[0];
        this.nationalities_en = t[1];
        this.setLang();
        console.log('pi', this.pi);
      });
  }

  ngAfterViewChecked(): void {
    this.setHeader();
    const c = this.nationalities.find(({nationality}) => nationality === this.personalInfo.secondNationality);
    if (c != null) {
      this.nationality = c;
    }
    console.log("nationality"+this.nationality?.nationality)


  }

  setModel() {
    if (sessionStorage.getItem('pi') !== null) {
      const session_pi = JSON.parse(sessionStorage.getItem('pi') || '');
      this.pi = session_pi;
      console.log("pi" + this.pi)
    } else {
      this.personalInfoService.findByRequestId(Number(this.reqId)).subscribe(data => {
        console.log("data", data)
        this.pi = data;
      });
    }

  }
  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 30;
  }

  setLang() {
    this.lang = localStorage.getItem('lang') || FRENCH_LABEL;
    this.setLabelsByLang(this.lang);

    this.translate.onLangChange.subscribe((data: any) => {
      console.log('onLangChange', data.lang);
      this.lang = data.lang;
      this.setLabelsByLang(data.lang);
    });
  }

  setLabelsByLang(lang: string) {
    if (lang === ENGLISH_LABEL) {
      this.pi.nationality = 'Tunisian';
      this.pi.secondNationality = 'French';
      this.nationality = { name: 'France', nationality: 'Française', flag: 'fr' };
      this.pi.maritalStatus = 'Single';
      this.civilities = this.civilities_en;
      this.nationalities = this.nationalities_en;
    }
    if (lang === FRENCH_LABEL) {
      this.pi.nationality = 'Tunisienne';
      this.pi.secondNationality = 'Française';
      this.nationality = { name: 'France', nationality: 'Française', flag: 'fr' };
      this.pi.maritalStatus = 'Célibataire';
      this.civilities = this.civilities_fr;
      this.nationalities = this.nationalities_fr;
    }

    this.pi.nbrkids = 0;
    this.pi.americanIndex = false;
  }

  setActive(index: number) {
    this.children.map(x => x.active = false);
    this.children[index].active = true;
    this.personalInfo.nbrkids = this.children[index].nb;

  }

  setActiveChild(){
    this.children.map(x => x.active = false);
    this.children[Number(this.personalInfo.nbrkids)].active = true;
   // this.pi.nbrkids = this.children[index].nb;
  }

  checkValue(event: any) {
    console.log(event);
  }

  onChangeNationality(nationality: any) {
    this.nationality = (this.lang === FRENCH_LABEL) ?
      this.nationalities_fr.find(item => item.nationality === nationality) :
      this.nationalities_en.find(item => item.nationality === nationality)


  }

  openDialog() {
    const dialogRef = this.dialog.open(FatcaDialogComponent);
    console.log(" this.personalInfo.americanIndex"+ this.personalInfo.americanIndex)

    dialogRef.afterClosed().subscribe((result: boolean) => {
      this.personalInfo.americanIndex = result;
      console.log(" this.personalInfo.americanIndex"+ this.personalInfo.americanIndex)
    });
  }

  continue() {
    this.personalInfoService.update(this.personalInfo).subscribe(data => {
      sessionStorage.setItem('pi', JSON.stringify(data));
      ls.set("idPersonalInfo", this.pi.id)
      ls.set("reqId",this.reqId)
    });
    /** to put inside subscribe once service c bon */
    this.router.navigate(['offer/summary']);
  }

  prevStep() {
    this.router.navigate(['offer/summary']);
  }



  getJSON(path: string): Observable<any> {
    return this.http.get(path);
  }
  secondNatio(event :any){
    console.log(event.target.checked)
   if(event.target.checked) {this.secondNAtio = false;
  }
    
  }

  getPersonalInfo(id:number){
    this.personalInfoService.findByPersonalId(id).subscribe(result=>{
    this.personalInfo=result;
    console.log((this.personalInfo.secondNationality))
  }, (error) => {
    console.log(error);
  },()=>{
    if(this.personalInfo.secondNationality!= null){
      this.secondNAtio = false;
    }
    this.setActiveChild()
    
  })
    }
}




import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CivilStatusUpdateComponent } from './civil-status-update.component';

describe('CivilStatusUpdateComponent', () => {
  let component: CivilStatusUpdateComponent;
  let fixture: ComponentFixture<CivilStatusUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CivilStatusUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CivilStatusUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

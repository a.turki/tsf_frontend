import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignatureContratComponent } from './signature-contrat.component';

describe('SignatureContratComponent', () => {
  let component: SignatureContratComponent;
  let fixture: ComponentFixture<SignatureContratComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignatureContratComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignatureContratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { RequestInfo } from 'src/app/shared/models/RequestInfo';
import { ls } from 'src/app/shared/settings/AppSettings';
declare var window: Window & typeof globalThis;
@Component({
  selector: 'app-signature-contrat',
  templateUrl: './signature-contrat.component.html',
  styleUrls: ['./signature-contrat.component.scss']
})
export class SignatureContratComponent implements OnInit {
  reqId = ls.get("reqId");
  signedDossierId: any;
  emailAdress: string | undefined;
  butSign: boolean = false;
  request: RequestInfo = new RequestInfo();
  offerId: any;
  pdfSrc = "";

  constructor(public router: Router, private personalInfoService: PersonalInfoService, public gsService: GlobalStateService) { }

  ngOnInit(): void {
    this.setHeader()
    this.addNewDoc(Number(this.reqId));
    console.warn(Number(this.reqId))
    this.getRequest(Number(this.reqId));
  }
  setHeader() {
    this.gsService.userChoice.personalInfo.activated = "active new_style";
    this.gsService.userChoice.offer.activated = "active";
    this.gsService.userChoice.progressBarValue = 90;
    this.gsService.userChoice._step = 'signatureContrat';
  }

  addNewDoc(id: any) {
    this.personalInfoService.AddDocument(id).subscribe((result) => {
    }, (error) => {
      console.log(error);
    }, () => { });
  }

  addDoc() {
    this.personalInfoService.goToSignDocument(Number(this.reqId), this.offerId).subscribe((result) => {
      console.log(result)
      this.signedDossierId = result.dossierId;
      this.emailAdress = result.emailAdress;
    }, (error) => {
      console.log(error);
    }, () => {
      this.redirectionToSignAttijari()
      console.log("signedDocId" + this.signedDossierId)
    });
  }

  public redirectionToSignAttijari(): void {
    //window.open('https://sign.attijaribank.com.tn/',"_self");

    let redirectionVarOne: string;
    let redirectionVarTow: string;
    let redirectionVarThree: string;
    redirectionVarOne = "https://sign.attijaribank.com.tn/pub/corporateSign?entId=f069295a-8fbe-41ab-8565-516d6e2caf10&dossierId="
    redirectionVarTow = "&signataire="
    redirectionVarThree = "&redirectUrl=";
    console.log('the signed dossier ', this.signedDossierId);
    //console.log('the url de redirection', redirectionVarOne + this.signedDossierId + redirectionVarTow + this.emailAdress + redirectionVarThree + this.reference)
    window.open(redirectionVarOne + this.signedDossierId + redirectionVarTow + this.emailAdress + redirectionVarThree + "http://localhost:4200/offer/validationCompte");
    console.log(redirectionVarOne + this.signedDossierId + redirectionVarTow + this.emailAdress + redirectionVarThree + "http://localhost:4200/offer/validationCompte")
    //https://sign.attijaribank.com.tn/pub/corporateSign?entId=ec45c5a4-b4e0-4375-b409-1833d722a72b&dossierId=fca6f879-e7e9-455c-a081-063ebcc52a2b&signataire=yassine.trigui@teamwill.fr&redirectUrl=https%3A%2F%2Fcreditconso.attijaribank.com.tn%2F
  }



  getDocumentSigned() {
    this.personalInfoService.getDocumentSigned(this.signedDossierId).subscribe((result) => {
    }, (error) => {
      console.log(error);
    }, () => { });
  }

  onConfirmCheck(event: any) {
    if (event.target.checked) {
      this.butSign = true;
    }
    else {
      this.butSign = false;
    }
  }

  downloadFile() {
    this.personalInfoService.downloadFile(this.offerId).subscribe((resp: any) => {
      // var ext = nameFile.substring(nameFile.lastIndexOf('.') + 1);
      //var file;
      console.log('resp' + resp)
      let file = new Blob([resp], { type: 'application/pdf' });
      console.log('resp' + file)
      var fileURL = URL.createObjectURL(file);
      var link = document.createElement('a');
      link.href = fileURL;
      if (this.offerId == 1) { link.download = "Bulletin de souscription pack BLEDI+.pdf"; }
      else if (this.offerId == 2) { link.download = "Bulletin de souscription PRIVILEGES BLEDI.pdf"; }
      else if (this.offerId == null) { link.download = "Convention de compte.pdf"; }
      //else(){}
      link.click();
      console.log('resp' + fileURL)
      var win: any = window.open();
      win.document.write('<iframe src="' + fileURL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>');
    }, (error) => {
      console.log('the getting of document was faild', error);
    }, () => { });
  }

  getRequest(id: number) {
    this.personalInfoService.getRequest(id).subscribe((data: RequestInfo) => {
      this.request = data;
      this.offerId = data.offerId;
      console.log(this.offerId)
    }, (error) => {
      console.log('the getting of idRequest was faild', error);
    }, () => {
      if (this.offerId == 1) {
        this.pdfSrc = "../../../assets/pdf/Bulletin de souscription pack BLEDI+.pdf";

      }
      else if (this.offerId == 2) {
        this.pdfSrc = "../../../assets/pdf/Bulletin de souscription PRIVILEGES BLEDI.pdf";
      }
      else  {
        this.pdfSrc = "../../../assets/pdf/Convention de compte.pdf";
      }

    });
  }
}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PersonalInfoService } from 'src/app/services/personalInfo/personal-info.service';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { PersonalInfo } from 'src/app/shared/models/personalInfo';
import { ls } from 'src/app/shared/settings/AppSettings';

@Component({
  selector: 'app-complete-request',
  templateUrl: './complete-request.component.html',
  styleUrls: ['./complete-request.component.scss']
})
export class CompleteRequestComponent implements OnInit {
  saved:boolean = false;
  reqId = ls.get("reqId");
  persoInfoId =ls.get("idPersonalInfo") ;
  personalInfo: PersonalInfo = new PersonalInfo();
  constructor(public gsService: GlobalStateService, public dialogRef: MatDialogRef<CompleteRequestComponent>,private personalInfoService: PersonalInfoService, public router: Router,
    public translate: TranslateService, private http: HttpClient) { }

  ngOnInit(): void {
    this.getPersonalInfo(Number(this.persoInfoId));
  }
  closeDrawer() {
    this.dialogRef.close();
  }
  saveDemand(){
    this.saved = true;
    this.personalInfoService.RequestRegistration(this.reqId, this.gsService.userChoice._step).subscribe(res=>{
      console.log(res);
    })
  }

  getPersonalInfo(id:number){
    this.personalInfoService.findByPersonalId(id).subscribe(result=>{
    this.personalInfo=result;
    console.log(this.personalInfo.email)
  })
    }
}

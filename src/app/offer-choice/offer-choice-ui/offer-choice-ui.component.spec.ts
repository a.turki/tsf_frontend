import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferChoiceUiComponent } from './offer-choice-ui.component';

describe('OfferChoiceUiComponent', () => {
  let component: OfferChoiceUiComponent;
  let fixture: ComponentFixture<OfferChoiceUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferChoiceUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferChoiceUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {
  AfterViewInit,
  Component,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import {GlobalStateService, HeaderButton} from 'src/app/services/tfsGlobal/global-state.service';
import { MatDialog } from '@angular/material/dialog';
import { CompleteRequestComponent } from '../complete-request/complete-request.component';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-offer-choice-ui',
  templateUrl: './offer-choice-ui.component.html',
  styleUrls: ['./offer-choice-ui.component.scss'],
})
export class OfferChoiceUiComponent implements OnInit, AfterViewInit {
  innerWidth: any;
  doTour = false;
  display: any;
  stepNumber: any;
  firstStepClass: any;
  secondStepClass: any;
  thirdStepClass: any;
  fourthStepClass: any;
  fifthStepClass: any;
  progressBarValue: any;
  headerButton!: HeaderButton;
  displayLogin: any;
  displaySaveIcon: any;
  constructor(public router: Router,
              public globalStateService: GlobalStateService,
              public dialog: MatDialog,
              private readonly joyrideService: JoyrideService) { }

  ngAfterViewInit(): void {
  }



  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.display = this.globalStateService.userChoice._step === 'offer_choice' ? 'none' : 'block';
    this.stepNumber = this.globalStateService.getStepNumber();
    this.firstStepClass = this.globalStateService.userChoice.offer.activated;
    this.secondStepClass = this.globalStateService.userChoice.personalInfo.activated;
    this.thirdStepClass = this.globalStateService.userChoice.recup;
    this.fourthStepClass = this.globalStateService.userChoice.justif;
    this.fifthStepClass = this.globalStateService.userChoice.sign;
    this.progressBarValue = this.globalStateService.userChoice.progressBarValue;
    this.headerButton = this.globalStateService.getHeaderButton(innerWidth);
    this.displayLogin = this.globalStateService.userChoice._step === 'offer_choice' ? 'block' : 'none';
    this.displaySaveIcon = this.globalStateService.userChoice._step === 'offer_choice' ? 'none' : 'block';
    if (this.innerWidth < 500) {
      if (this.globalStateService.userChoice._step === 'civil-status') {
        this.doTour = true;
      }
    }
    if (this.doTour) {
      this.joyrideService.startTour(

        {
          steps: ['firstStep'],
          themeColor: '#404040',
          showCounter: false,
        } // Your steps order
      );
    }
  }


  redirect(routerName: any): void {
    switch (routerName) {
      case 'ouvrircomptesimple': {
        this.router.navigate(['offer/step2Simple']);
        break;
      }
      case 'mailValidation': {
        const dialogCompleteCreditRequest = this.dialog.open(CompleteRequestComponent, {
          width: this.innerWidth > 800 ? '38%' : '100%',
          minWidth: this.innerWidth > 800 ? '38%' : '100%',
          height: '100%',
          position: { left: this.innerWidth > 800 ? '62%' : '0%' },
          role: 'dialog',
          autoFocus: false,
          data: {}, panelClass: ['animate__animated', 'animate__slideInRight']
        });
        break;
      }
      default: {
        // statements;
        break;
      }
    }

  }

  homeRedirection() {
    this.router.navigate(['']);
  }

}

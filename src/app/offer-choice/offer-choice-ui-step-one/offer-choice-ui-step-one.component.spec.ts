import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferChoiceUiStepOneComponent } from './offer-choice-ui-step-one.component';

describe('OfferChoiceUiStepOneComponent', () => {
  let component: OfferChoiceUiStepOneComponent;
  let fixture: ComponentFixture<OfferChoiceUiStepOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferChoiceUiStepOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferChoiceUiStepOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

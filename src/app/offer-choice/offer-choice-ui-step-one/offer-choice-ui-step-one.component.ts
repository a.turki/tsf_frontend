import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalStateService } from 'src/app/services/tfsGlobal/global-state.service';
import { Offer } from 'src/app/shared/models/offer';
import { OfferService } from '../../services/offer/offer.service';
@Component({
  selector: 'app-offer-choice-ui-step-one',
  templateUrl: './offer-choice-ui-step-one.component.html',
  styleUrls: ['./offer-choice-ui-step-one.component.scss']
})
export class OfferChoiceUiStepOneComponent implements OnInit, OnDestroy {
  offers: Offer[] = [];
  constructor(private router: Router , private offerService: OfferService, public globalStateService: GlobalStateService) {
   }


  ngOnInit(): void {
    console.log(this.globalStateService.userChoice.progressBarValue);
    this.getOffers();
    this.setHeader();
  }

  getOffers() {
    this.offerService.getOffers().subscribe((data) => {
      this.offers = data;
    });
  }

  packChoiceToNextStep(item: Offer){

    this.globalStateService.userChoice.offer.id = item.id;
    this.globalStateService.userChoice.offer.detailOffers = item.detailOffers;
    this.globalStateService.userChoice.offer.name = item.name;
    this.globalStateService.userChoice.offer.price = item.price;
    this.globalStateService.userChoice.offer.url = item.url;

    this.router.navigate(['offer/step2']);
  }

  ngOnDestroy(): void {

  }
  setHeader(): void {
    this.globalStateService.userChoice.offer.activated = 'active new_style';
    this.globalStateService.userChoice.progressBarValue = 10;
    this.globalStateService.userChoice._step = 'offer_choice';
  }
}
